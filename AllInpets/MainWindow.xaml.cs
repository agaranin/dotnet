﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btRegiserMe_Click(object sender, RoutedEventArgs e)
        {
            String inpData;
            String age = "";
            if (rbAge0_18.IsChecked == true) age = (string)rbAge0_18.Content;
            if (rbAge18_35.IsChecked == true) age = (string)rbAge18_35.Content;
            if (rbAge36up.IsChecked == true) age = (string)rbAge36up.Content;
            var pets = new List<string>();
            if (cbCat.IsChecked.Value) pets.Add((string)cbCat.Content);
            if (cbDog.IsChecked.Value) pets.Add((string)cbDog.Content);
            if (cbOther.IsChecked.Value) pets.Add((string)cbOther.Content);
            inpData = ($"{tbName.Text};{age};{String.Join(",", pets)};{comboConts.Text};{sliderTemp.Value}");

            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"..\..\data.txt", true))

                {
                    file.WriteLine(inpData);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error file writting");
            }
            lstInputs.Items.Add(inpData);
        }
    }
}
