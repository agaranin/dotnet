﻿using Day01PeopleListInFile;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClassPersonUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GettersReturnValuesPovidedToTheConstructor()
        {
            Person person = new Person(30, "Montreal", "Artem");
            Assert.AreEqual<int>(30, person.Age);
            Assert.AreEqual<string>("Montreal", person.City);
            Assert.AreEqual<string>("Artem", person.Name);

        }
    }
}
