﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01ArrayContains
{
    class Program
    {
        public int[] Concatenate(int[] a1, int[] a2)
        {
            var result = new int[a1.Length + a2.Length];
            for (int i = 0; i < a1.Length; i++)
            {
                result[i] = a1[i];
            }
            int k = 0;
            for (int j = a1.Length; j < result.Length; j++)
            {
                result[j] = a2[k];
                k++;
            }
            return result;
        }

        public static void PrintDups(int[] a1, int[] a2)
        {
            for (int i = 0; i < a1.Length; i++)
            {
                for (int j = 0; j < a2.Length; j++)
                {
                    if (a1[i].Equals(a2[j]))
                    {
                        Console.WriteLine(a1[i]);
                    }
                }
            }

        }

        public int[] RemoveDups(int[] a1, int[] a2)
        {
            String res = "";
            for (int i = 0; i < a1.Length; i++)
            {
                for (int j = 0; j < a2.Length; j++)
                {
                    if (a1[i].Equals(a2[j]))
                    {
                        res += String.Concat(a1[i] + " ");
                    }
                }
            }
            int[] dups = new int[res.Trim().Split(' ').Length];
            for (int i = 0; i < dups.Length; i++)
            {
                dups[i] = Convert.ToInt32(res.Trim().Split(' ')[i]);
            }
            int[] result = new int[a1.Length - dups.Length];
            bool flag = false;
            int k = 0;
            for (int i = 0; i < a1.Length; i++)
            {
                for (int j = 0; j < a2.Length; j++)
                {
                    if (a1[i] == a2[j])
                    {
                        flag = true;
                    }
                }
                if (flag == false)
                {
                    result[k] = a1[i];
                    k++;
                }
                flag = false;
            }
            return result;
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            int[] a1 = { 2, 7, 8, 11, -6 };
            int[] a2 = { -2, 9, -6, 2, 11, };
            int[] res = p.Concatenate(a1, a2);
            Console.WriteLine("Part 1: ");
            for (int i = 0; i < res.Length; i++)
            {
                Console.Write(res[i] + ", ");
            }
            Console.WriteLine("\nPart 2: ");
            PrintDups(a1, a2);


            Console.WriteLine("\nPart 3: ");
            int[] resPart3 = p.RemoveDups(a1, a2);
            for (int i = 0; i < resPart3.Length; i++)
            {
                Console.Write(resPart3[i] + ", ");
            }
            Console.WriteLine("\nPress any key");
            Console.ReadKey();
        }
    }
}
