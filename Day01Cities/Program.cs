﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{

    class City
    {
        public string Name;
        public double PopulationMillions;
        public override string ToString()
        {
            return string.Format("City: {0} with {1} mil. population", Name, PopulationMillions);
        }
    }

    class BetterCity
    {
        public string Name { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                City c1 = new City();
                c1.Name = "Montreal";
                Console.WriteLine(c1);

                City c2 = new City { Name = "Toronto", PopulationMillions = 4.5 };
                BetterCity bc1 = new BetterCity();
                bc1.Name = "Vancouver";
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}
