﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cone
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How big does your (ice cream) cone needs to be?");
            int size = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < size; i++)
            {
                for (int k = 0; k < i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < 2 * (size - i) - 1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("");
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
