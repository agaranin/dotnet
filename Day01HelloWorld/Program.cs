﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("What is your name?:");
                string name = Console.ReadLine();
                Console.Write("How old are you?");
                int age = int.Parse(Console.ReadLine());
                Console.WriteLine("Hello {0}, you are {1} y/o, nice to meet you {0}", name, age);
                Console.WriteLine("Hello " + name + " you are " + age + " y/o, nice to meet you.");
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Input is wrong.");
            }

            Console.WriteLine("Press any key to finish");
            Console.ReadKey();
        }
    }
}
