﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01NamesAgain
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] nameList = new String[5];
            for (int i = 0; i < nameList.Length; i++)
            {
                Console.WriteLine("Enter a name: ");
                nameList[i] = Console.ReadLine();
            }
            Console.WriteLine("Enter search string: ");
            string search = Console.ReadLine();
            for (int i = 0; i < nameList.Length; i++)
            {
                if (nameList[i].Contains(search))
                    Console.WriteLine("Matching name: "+nameList[i]);
            }
            String longest = nameList[0];
            for (int i = 0; i < nameList.Length; i++)
            {
                if (nameList[i].Length > longest.Length)
                    longest = nameList[i];
            }
            Console.WriteLine("Longest name is: " + longest);

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
