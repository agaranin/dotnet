﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Console.WriteLine("How many numbers do you want generate?");
            int count = Convert.ToInt32(Console.ReadLine());
            var numList = new List<int>();
            for (int i = 0; i <= count; i++)
            {
                numList.Add(rnd.Next(-100, 101));
            }
            foreach (var item in numList)
            {
                if (item<=0)
                {
                    Console.WriteLine(item);
                }
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
