﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01People
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] namesArray = new String[5];
            int[] agesArray = new int[5];
            int youngest = 1000;
            string name = null;
            for (int i = 1; i < 5; i++)
            {
                Console.Write("Enter name of person #" + i + ":");
                namesArray[i] = Console.ReadLine();
                Console.Write("Enter age  of person #" + i + ":");
                agesArray[i] = int.Parse(Console.ReadLine());
                if (youngest > agesArray[i])
                {
                    youngest = agesArray[i];
                    name = namesArray[i];
                }
            }
            Console.WriteLine($"\nYoungest person is {youngest} and their name is {name}");
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
