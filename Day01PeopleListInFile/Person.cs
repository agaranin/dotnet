﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person
    {
        private string _name; // Name 2-100 characters long, not containing semicolons
        private int _age; // Age 0-150
        private string _city; // City 2-100 characters long, not containing semicolons

        public Person(int age, string city, string name)
        {
            Age = age;
            City = city;
            Name = name;
        }
        public Person()
        {

        }

        public T Sum<T>(ref T x, ref T y)
        {
            List<T> list = new List<T>();
            list.Add(x);
            list.Add(y);
            var result = list.Sum();
            return result;
        }
        public int Age
        {
            get => _age; set
            {
                if (value > 150 || value < 0)
                {
                    throw new ArgumentException("Age must be between 0 and 150 ");
                }
                _age = value;
            }
        }
        public string City
        {
            get => _city; set
            {
                if (!Regex.Match(value, @"\A[^;]{2,100}\Z").Success)
                {
                    throw new ArgumentException("City must be between 2 and 100 characters, not containing semicolons ");
                }
                _city = value;
            }
        }

        public string Name
        {
            get => _name; set
            {
                if (!Regex.Match(value, @"\A[^;]{2,100}\Z").Success)
                {
                    throw new ArgumentException("Name must be between 2 and 100 characters, not containing semicolons ");
                }
                _name = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} is {1} from {2}", Name, Age, City);
        }
        public string ToDataString()
        {
            return String.Format("{0},{1},{2}", Name, Age, City);
        }
    }
}
