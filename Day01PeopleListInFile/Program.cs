﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{

    class Program
    {
        const string fileName = @"..\..\people.txt";
        static string filePath = Path.Combine(Environment.CurrentDirectory, fileName);

        static void LoadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(filePath))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            string[] splitStr = ln.Split(',');
                            if (splitStr.Length != 3)
                            {
                                errorsList.Add("Wrong structure in line " + counter);
                                errorsList.Add("=> " + ln);
                                continue;
                            }
                            String name = splitStr[0];
                            int age = Convert.ToInt32(splitStr[1]);
                            String city = splitStr[2];
                            people.Add(new Person(age, city, name));
                        }
                        catch (FormatException)
                        {
                            errorsList.Add("Age must be a number in line " + counter);
                            errorsList.Add("=> " + ln);
                            continue;
                        }
                        catch (ArgumentException ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter);
                            errorsList.Add("=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    { // show errors to the user
                        Console.WriteLine(String.Join("\n", errorsList));
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Console.Clear();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }

        static void SaveDataToFile()
        {
            using (TextWriter tw = new StreamWriter(filePath))
            {
                foreach (Person p in people)
                {
                    tw.WriteLine(p.ToDataString());
                }
            }
        }
        static void AddPersonInfo()
        {
            try
            {
                Console.Write("Enter name: ");
                string name = Console.ReadLine();
                Console.Write("Enter age: ");
                int age = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter city: ");
                string city = Console.ReadLine();
                people.Add(new Person(age, city, name));
                Console.WriteLine("Person added.");
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Age should be a number: " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ListAllPersonsInfo()
        {
            foreach (Person p in people)
            {
                Console.WriteLine(p);
            }
        }
        static void FindPersonByName()
        {
            Console.Write("Enter partial person name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Matches found: ");
            foreach (Person p in people)
            {
                if (p.Name.ToLower().Contains(name.ToLower()))
                    Console.WriteLine(p);
            }
        }
        static void FindPersonYoungerThan()
        {
            Console.Write("Enter maximum age: ");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Matches found: ");
            foreach (Person p in people)
            {
                if (p.Age <= age)
                    Console.WriteLine(p);
            }
        }

        static int GetMenuChoice()
        {
            Console.Write(
@"1. Add person info
2. List persons info
3. Find a person by name
4. Find all persons younger than age
0. Save & Exit
Enter your choice: ");
            try
            {
                int choice = Convert.ToInt32(Console.ReadLine());
                return choice;
            }
            catch (FormatException)
            {
                Console.WriteLine("Enter number between 0 to 4 ");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                Console.Clear();
                return GetMenuChoice();
            }
        }

        static List<Person> people = new List<Person>();
        static void Main(string[] args)
        {
            LoadDataFromFile();
            while (true)
            {
                int choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveDataToFile();
                        Console.WriteLine("Data saved. Exiting");
                        return;
                    default:
                        Console.WriteLine("Internal error: invalid control flow in menu");
                        break;
                }
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                Console.Clear();
            }

        }

    }
}
