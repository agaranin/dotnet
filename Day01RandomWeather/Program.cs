﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01RandomWeather
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int temp = rnd.Next(-30, 31);
            if (temp <= -15)
            {
                Console.WriteLine($"Temperature is {temp}. Very very cold");
            }
            else if (temp > -15 && temp < 0)
            {
                Console.WriteLine($"Temperature is {temp}. Freezing already");
            }
            else if (temp >= 0 && temp <= 15)
            {
                Console.WriteLine($"Temperature is {temp}. Spring or Fall");
            }
            else if (temp > 15)
            {
                Console.WriteLine($"Temperature is {temp}. That's what I like");
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
