﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
	class Person
	{
        public Person(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 3)
            {
                throw new InvalidParameterExcception("Invalid number of fields");            
            }
            if (data[0] != "Person")
            {   
                throw new InvalidParameterExcception("Person does not define");            
            }
            Name = data[1];
            int age;
            if (!int.TryParse(data[2], out age))
            { 
                throw new InvalidParameterExcception("Age must be an integer value");            
            }
            Age = age;
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        protected Person()
        {
        }

        string _name; // 1-50 characters, no semicolons
		int _age; // 0-150


        public string Name
        {
            get => _name; set
            {
                if (!Regex.Match(value, @"\A[^;]{1,50}\Z").Success)
                {
                    throw new InvalidParameterExcception("Name must be between 1 and 50 characters, no semicolons ");
                }
                _name = value;
            }
        }
        public int Age
        {
            get => _age; set
            {
                if (value > 150 || value < 0)
                {
                    throw new InvalidParameterExcception("Age must be between 0 and 150 ");
                }
                _age = value;
            }
        }


        public override string ToString()
        {
            return string.Format("Person {0} is {1} y/o", Name, Age);
        }

        public virtual string ToDataString()
        {
            return string.Format("Person;{0};{1}",Name,Age);
        }

    }

}
