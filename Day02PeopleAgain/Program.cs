﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{

    class Program
    {
        public delegate void LogFailedSetterDelegate(string reason);
        public static LogFailedSetterDelegate LogFailSet;
        static List<Person> peopleList = new List<Person>();
        const string fileName = @"..\..\people.txt";
        const string fileLogName = @"..\..\log.txt";
        static string filePath = Path.Combine(Environment.CurrentDirectory, fileName);
        static string fileLogPath = Path.Combine(Environment.CurrentDirectory, fileLogName);

        static void reportErrorsToFile(string reason)
        {
            try
            {
                File.AppendAllText(fileLogPath, reason + "\n");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void reportErrorsOnScreen(string reason)
        {
            Console.WriteLine(reason);
        }

        static void SaveDataToFile()
        {

            try
            {
                using (TextWriter tw = new StreamWriter(filePath))
                {
                    peopleList = peopleList.OrderBy(p => p.Name).ToList();
                    foreach (var p in peopleList)
                    {
                        tw.WriteLine(p.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {

                throw;
            }
        }
        static void LoadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(filePath))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            string[] splitStr = ln.Split(';');
                            if (splitStr.Length != 3 && splitStr.Length != 5)
                            {
                                errorsList.Add("Wrong structure in line " + counter + "\n=> " + ln);
                                continue;
                            }
                            if (splitStr[0] == "Person")
                            {
                                peopleList.Add(new Person(ln));
                            }
                            else if (splitStr[0] == "Teacher")
                            {
                                peopleList.Add(new Teacher(ln));
                            }
                            else if (splitStr[0] == "Student")
                            {
                                peopleList.Add(new Student(ln));
                            }
                            else
                            {
                                errorsList.Add("Unknown object in line " + counter + "\n=> " + ln);
                                continue;
                            }

                        }
                        catch (InvalidParameterExcception ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    { // show errors to the user
                        //Console.WriteLine("List of found errors:");
                        //Console.WriteLine(String.Join("\n", errorsList));
                        LogFailSet?.Invoke(String.Join("\n", errorsList));
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        Console.Clear();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }
        static int GetMenuChoice()
        {
            //while (true)
            //{
            Console.Write(
@"Where would you like to log setters errors?
1-screen only
2-screen and file
3-do not log
Your choice: ");
            string choiceStr = Console.ReadLine();
            Console.Clear();
            int choice;
            if (!int.TryParse(choiceStr, out choice) || choice < 1 || choice > 3)
            {
                return 1;
                //Console.WriteLine("Value must be a number between 1-3");
                //continue;
            }
            return choice;
            //}
        }

        static List<double> GetGpaList(List<Person> peopleList)
        {
            var gpaList = new List<double>();
            Student student = null;
            foreach (var p in peopleList)
            {
                if (p is Student)
                {
                    student = (Student)p;
                    gpaList.Add(student.GPA);
                }
            }
            return gpaList;
        }
        static double CalculateAverageGPA(List<Person> peopleList)
        {
            // calculate average student's GPA
            var gpaList = new List<double>();
            double gpaAverage = 0;
            Student student = null;
            foreach (var p in peopleList)
            {
                if (p.GetType().Name == "Student")
                {
                    if (p is Student)
                    {
                        student = (Student)p;
                    }
                    gpaList.Add(student.GPA);
                    gpaAverage += student.GPA;
                }
            }
            return gpaAverage / gpaList.Count;
        }

        static double CalculateMedianGPA(List<Person> peopleList)
        {
            // calculate median student's GPA
            List<double> gpaList = GetGpaList(peopleList);
            gpaList.Sort();
            if (gpaList.Count % 2 != 0) // for an ODD set
            {
                int index = (gpaList.Count + 1) / 2;
                double medianGPA = gpaList[index - 1];
                return medianGPA;
            }
            else // for an EVEN set
            {
                int index1 = gpaList.Count / 2;
                int index2 = gpaList.Count / 2 + 1;
                double medianGPA = (gpaList[index1 - 1] + gpaList[index2 - 1]) / 2;
                return medianGPA;
            }
        }

        static double CalculateGpaDeviation(List<Person> peopleList)
        {
            // calculate standard deviation of student's GPA
            var devList = new List<double>();
            double eachDev = 0;
            double variance = 0;
            double standartDeviation = 0;
            foreach (double gpa in GetGpaList(peopleList))
            {
                eachDev = Math.Pow((gpa - CalculateAverageGPA(peopleList)), 2);
                devList.Add(eachDev);
                variance += eachDev;
            }
            variance = variance / devList.Count;
            // standartDeviation = Math.Round(Math.Sqrt(variance),2);
            standartDeviation = Math.Sqrt(variance);
            return standartDeviation;
        }

        static void Main(string[] args)
        {
            int choice = GetMenuChoice();
            switch (choice)
            {
                case 1:
                    LogFailSet = reportErrorsOnScreen;
                    break;
                case 2:
                    LogFailSet = reportErrorsOnScreen;
                    LogFailSet += reportErrorsToFile;
                    break;
                case 3:
                    LogFailSet = null;
                    break;
                default:
                    Console.WriteLine("Internal error: invalid control flow in menu");
                    break;
            }
            LoadDataFromFile();
            Console.WriteLine("List of People:");
            foreach (var p in peopleList)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine("\nList of students:");
            foreach (var p in peopleList)
            {
                if (p.GetType() == typeof(Student))
                    Console.WriteLine(p);
            }
            Console.WriteLine("\nList of Teachers:");
            foreach (var p in peopleList)
            {
                if (p.GetType() == typeof(Teacher))
                    Console.WriteLine(p);
            }
            Console.WriteLine("\nList of Persons:");
            foreach (var p in peopleList)
            {
                if (p.GetType() == typeof(Person))
                    Console.WriteLine(p);
            }

            Console.WriteLine("\nAverage student's GPA: " + CalculateAverageGPA(peopleList));

            Console.WriteLine("\nMedian student's GPA: " + CalculateMedianGPA(peopleList));

            Console.WriteLine("\nStandard deviation of student's GPA: " + CalculateGpaDeviation(peopleList));

            // Order list by name
            //Console.WriteLine("\nList of People sorted by name:");
            //peopleList = peopleList.OrderBy(p => p.Name).ToList();
            // peopleList = peopleList.OrderBy(p => p.Name).ThenBy(p => p.Age) .ToList(); // sorting by two field
            // peopleList = peopleList.Where(p => p.Age > 20 && p.Name.Length > 5).ToList(); // filtering by age and name length

            //int yearsTotal = 0;
            //yearsTotal = peopleList.Sum(p => p.Age); // The sum of ages
            //yearsTotal = peopleList.Where(p => p.Age > 40).Sum(p => p.Age); // sum + filtering
            //Console.WriteLine($"The sum of all ages is {yearsTotal}");
            //foreach (var p in peopleList)
            //{
            //    Console.WriteLine(p);
            //}

            Console.WriteLine("\nPress any key to save and exit");
            SaveDataToFile();
            Console.ReadKey();
        }
    }
}
