﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Student : Person
    {

        string _program; // 1-50 characters, no semicolons
        double _gPA; // 0-4.3

        public Student(string dataLine) : base()
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterExcception("Invalid number of fields");
            }
            if (data[0] != "Student")
            {
                throw new InvalidParameterExcception("Student does not define");
            }
            Name = data[1];
            int age;
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterExcception("Age must be an integer value");
            }
            Age = age;
            Program = data[3];
            double gpa;
            if (!double.TryParse(data[4], out gpa))
            {
                throw new InvalidParameterExcception("GPA must be a double value");
            }
            GPA = gpa;
        }

        public Student(string name, int age, string program, double gPA) : base(name, age)
        {
            Program = program;
            GPA = gPA;
        }

        public string Program
        {
            get => _program; set
            {
                if (!Regex.Match(value, @"\A[^;]{1,50}\Z").Success)
                {
                    throw new InvalidParameterExcception("Program must be between 1 and 50 characters, no semicolons ");
                }
                _program = value;
            }
        }
        public double GPA
        {
            get => _gPA; set
            {
                if (value > 4.3 || value < 0)
                {
                    throw new InvalidParameterExcception("GPA must be between 0 and 4.3 ");
                }
                _gPA = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Student {0} is {1} y/o, studies {2} with {3} GPA", Name, Age, Program, GPA);
        }
        public override string ToDataString()
        {
            return string.Format("Student;{0};{1};{2};{3}", Name, Age, Program, GPA);
        }
    }
}
