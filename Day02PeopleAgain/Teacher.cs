﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Teacher : Person
    {
        public Teacher(string dataLine) : base()
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterExcception("Invalid number of fields");
            }
            if (data[0] != "Teacher")
            {
                throw new InvalidParameterExcception("Teacher does not define");
            }
            Name = data[1];
            int age;
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterExcception("Age must be an integer value");
            }
            Age = age;
            Subject = data[3];
            int yoe;
            if (!int.TryParse(data[4], out yoe))
            {
                throw new InvalidParameterExcception("Years of experiensce must be an integer value");
            }
            YearsOfExperience = yoe;
        }

        

        public Teacher(string name, int age, string subject, int yearsOfExperience) : base(name, age)
        {
            Subject = subject;
            YearsOfExperience = yearsOfExperience;
        }

        string _subject; // 1-50 characters, no semicolons
        int _yearsOfExperience; // 0-100
        public string Subject
        {
            get => _subject; set
            {
                if (!Regex.Match(value, @"\A[^;]{1,50}\Z").Success)
                {
                    throw new InvalidParameterExcception("Subject must be between 1 and 50 characters, no semicolons ");
                }
                _subject = value;
            }
        }
        public int YearsOfExperience
        {
            get => _yearsOfExperience; set
            {
                if (value > 100 || value < 0)
                {
                    throw new InvalidParameterExcception("YearsOfExperience must be between 0 and 100 ");
                }
                _yearsOfExperience = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Teacher {0} is {1} y/o teaches {2} for {3} years", Name, Age, Subject, YearsOfExperience);
        }

        public override string ToDataString()
        {
            return string.Format("Teacher;{0};{1};{2};{3}", Name, Age, Subject, YearsOfExperience);
        }
    }
}
