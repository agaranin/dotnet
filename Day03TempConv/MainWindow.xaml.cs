﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public void TempCalc()
        {
            if (rbOuputKelvin == null)
            {
                return;
            }
            if (!double.TryParse(tbInput.Text, out double temp))
            {
                lblOutput.Content = "Invalid value";
                return;
            }
            if (rbInputCelcius.IsChecked == true)
            {
                lblOutput.Content = $"{temp:F2}°C";
                if (rbOuputFahrenheit.IsChecked == true)
                {
                    temp = (temp * 1.8 + 32);
                    lblOutput.Content = $"{temp:F2}°F";
                }
                if (rbOuputKelvin.IsChecked == true)
                {
                    temp = temp + 273.15;
                    lblOutput.Content = $"{temp:F2}°K";
                }

            }
            if (rbInputFahrenheit.IsChecked == true)
            {
                lblOutput.Content = $"{temp:F2}°F";
                if (rbOuputCelcius.IsChecked == true)
                {
                    temp = (temp - 32) * 5 / 9;
                    lblOutput.Content = $"{temp:F2}°C";
                }
                if (rbOuputKelvin.IsChecked == true)
                {
                    temp = (temp - 32) * 5 / 9 + 273.15;
                    lblOutput.Content = $"{temp:F2}°K";
                }
            }
            if (rbInputKelvin.IsChecked == true)
            {
                    lblOutput.Content = $"{temp:F2}°K";
                if (rbOuputCelcius.IsChecked == true)
                {
                    temp = temp - 273.15;
                    lblOutput.Content = $"{temp:F2}°C";
                }
                if (rbOuputFahrenheit.IsChecked == true)
                {
                    temp = (temp - 273.15) * 1.8 + 32;
                    lblOutput.Content = $"{temp:F2}°F";
                }
            }
            
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            TempCalc();
        }
        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            TempCalc();
        }

        private void radioButtonChanged(object sender, RoutedEventArgs e)
        {
            TempCalc();
        }

    }
}
