﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04GridView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Person> PeopleList = new List<Person>();
        const string pattern = "yyyy-MM-dd";
        public MainWindow()
        {
            InitializeComponent();
            cbEnum.ItemsSource = Enum.GetValues(typeof (Sex));
        }

        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text == "")
            {
                MessageBox.Show("Enter tne name");
                return;
            }
            if (!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age must a number");
                return;
            }
            if (!DateTime.TryParseExact(tbDob.Text, pattern, null,
                                   DateTimeStyles.None, out DateTime date))
            {
                MessageBox.Show("Wrong date format");
                return;
            }
            PeopleList.Add(new Person(tbName.Text, age, (Sex)cbEnum.SelectedItem, date));
            lvPersons.ItemsSource = PeopleList;
            lvPersons.Items.Refresh();
            CleanInputs();
        }

        private void btnUpdatePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                Person p = (Person)lvPersons.SelectedItem;
                if (tbName.Text == "") 
                {
                    MessageBox.Show("Enter tne name");
                    return;
                }
                p.Name = tbName.Text;
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Age must be a number");
                    return;
                }
                if (!DateTime.TryParseExact(tbDob.Text, pattern, null,
                                   DateTimeStyles.None, out DateTime date))
                {
                    MessageBox.Show("Wrong date format");
                    return;
                }
                p.Age = age;
                p.Sex = (Sex)cbEnum.SelectedItem;
                p.Dob = date;
                lvPersons.Items.Refresh();
                CleanInputs();
            }
        }

        private void lvPersons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                Person p = (Person)lvPersons.SelectedItem;
                tbAge.Text = p.Age + "";
                tbName.Text = p.Name;
                cbEnum.SelectedItem = p.Sex;
                tbDob.Text = p.Dob.ToString("yyyy-MM-dd");
            }
            else
            {
                CleanInputs();
            }
        }

        private void CleanInputs() 
        {
            tbName.Text = "";
            tbAge.Text = "";
            tbDob.Text = "";
            cbEnum.SelectedIndex = 0;
        }

        private void btnDeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                PeopleList.RemoveAt(lvPersons.SelectedIndex);
                lvPersons.Items.Refresh();
                CleanInputs();
            }
        }
    }
}
