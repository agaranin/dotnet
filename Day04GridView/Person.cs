﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04GridView
{
    public enum Sex { male, female, other };
    class Person
    {

        string _name;
        int _age;
        Sex _sex;
        DateTime _dob;

        public Person(string name, int age, Sex sex, DateTime dob)
        {
            Name = name;
            Age = age;
            Sex = sex;
            Dob = dob;
        }

        public string Name
        {
            get => _name; set
            {
                _name = value;
            }
        }
        public int Age
        {
            get => _age; set
            {
                _age = value;
            }
        }

        public Sex Sex
        {
            get => _sex; set
            {
                _sex = value;
            }
        }

        public DateTime Dob
        {
            get => _dob;

            set
            {
                _dob = value;
            }
        }

        public override string ToString()
        {
            return $"{Sex} {Name} is {Age} years old";
        }
    }
}
