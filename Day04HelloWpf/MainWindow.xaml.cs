﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04HelloWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_SayHelloViaLabelClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string greeting = string.Format($"Hi {name}, nice to meet you!");
            lblGreeting.Content = greeting;
        }

        private void Button_SayHelloViaMsgBoxClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string greeting = string.Format($"Hi {name}, nice to meet you!");
            MessageBox.Show(greeting);
        }
    }
}
