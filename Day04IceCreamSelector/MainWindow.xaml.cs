﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04IceCreamSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static ArrayList FlavoursList = new ArrayList();
        public MainWindow()
        {
            InitializeComponent();
        }

        private ArrayList LoadFlavoursListBox()
        {
            ArrayList itemList = new ArrayList();
            itemList.Add("Vanilla");
            itemList.Add("Chocolate");
            itemList.Add("Strawberry");
            itemList.Add("Peach");
            return itemList;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (LstFlavours.SelectedIndex == -1) 
            {
                MessageBox.Show("Select flavour!");
                return;
            }
            string currentItemText = LstFlavours.SelectedValue.ToString();
            LstSelected.Items.Add(currentItemText);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            FlavoursList = LoadFlavoursListBox();
            LstFlavours.ItemsSource = FlavoursList;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (LstSelected.SelectedIndex == -1)
            {
                MessageBox.Show("Select flavour to delete!");
                return;
            }
            LstSelected.Items.RemoveAt(LstSelected.Items.IndexOf(LstSelected.SelectedItem));
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            LstSelected.Items.Clear();
        }
    }
}
