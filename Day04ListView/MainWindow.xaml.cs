﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04ListView
{
    public partial class MainWindow : Window
    {
        static List<Person> PeopleList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text == "")
            {
                MessageBox.Show("Enter tne name");
                return;
            }
            if (!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age must be a number");
                return;
            }
            PeopleList.Add(new Person(tbName.Text, age));
            lvPersons.ItemsSource = PeopleList;
            lvPersons.Items.Refresh();
            tbName.Text = "";
            tbAge.Text = "";
        }

        private void btnUpdatePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                if (tbName.Text == "")
                {
                    MessageBox.Show("Enter tne name");
                    return;
                }
                Person p = (Person)lvPersons.SelectedItem;
                p.Name = tbName.Text;
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Age must a number");
                    return;
                }
                p.Age = age;
                lvPersons.Items.Refresh();
            }
        }

        private void lvPersons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                Person p = (Person)lvPersons.SelectedItem;
                tbAge.Text = p.Age + "";
                tbName.Text = p.Name;
            }
            else
            {
                tbName.Text = "";
                tbAge.Text = "";
            }
        }

        private void btnDeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPersons.SelectedIndex != -1)
            {
                PeopleList.RemoveAt(lvPersons.SelectedIndex);
                lvPersons.Items.Refresh();
                tbName.Text = "";
                tbAge.Text = "";
            }
        }
    }
}
