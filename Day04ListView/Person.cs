﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04ListView
{
    class Person
    {

        string _name;
        int _age;

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public string Name
        {
            get => _name; set
            {
                _name = value;
            }
        }
        public int Age
        {
            get => _age; set
            {
                _age = value;
            }
        }

        public override string ToString()
        {
            return $"{Name} is {Age} years old";
        }
    }
}
