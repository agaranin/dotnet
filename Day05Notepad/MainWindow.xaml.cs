﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string FileName;
        static string FullFileName;
        static bool isModified = false;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void miOpen_Click(object sender, RoutedEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show($"Do you want to save changes to {FullFileName}?", "Notepad", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        File.WriteAllText(FullFileName, tbTxtEditor.Text);
                        isModified = false;
                        window.Title = FileName;
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                }
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                FullFileName = openFileDialog.FileName;
                tbTxtEditor.Text = File.ReadAllText(openFileDialog.FileName);
                lblStatusBar.Text = FullFileName;
                FileName = openFileDialog.SafeFileName;
                Title = openFileDialog.SafeFileName;

            }
        }

        private void miSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, tbTxtEditor.Text);
            }
        }

        private void miSave_Click(object sender, RoutedEventArgs e)
        {
            File.WriteAllText(FullFileName, tbTxtEditor.Text);
            isModified = false;
            Title = FileName;
        }

        private void tbTxtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            isModified = true;
            if (!Title.Contains("Mofified"))
            Title += " (Mofified)";
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show($"Do you want to save changes to {FullFileName}?", "Notepad", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        File.WriteAllText(FullFileName, tbTxtEditor.Text);
                        isModified = false;
                        window.Title = FileName;
                        Application.Current.Shutdown();
                        break;
                    case MessageBoxResult.No:
                        Application.Current.Shutdown();
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                } 
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        private void window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isModified)
            {
                MessageBoxResult result = MessageBox.Show($"Do you want to save changes to {FullFileName}?", "Notepad", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        File.WriteAllText(FullFileName, tbTxtEditor.Text);
                        isModified = false;
                        window.Title = FileName;
                        Application.Current.Shutdown();
                        break;
                    case MessageBoxResult.No:
                        Application.Current.Shutdown();
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
            else
            {
                Application.Current.Shutdown();
            }
        }
    }
}
