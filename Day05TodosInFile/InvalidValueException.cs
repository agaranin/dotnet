﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05TodosInFile
{

    class InvalidValueExcception : Exception
    {
        public InvalidValueExcception(string msg) : base(msg) { }
    }
}
