﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05TodosInFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Todo> TaskList = new List<Todo>();
        const string DatePattern = "y-M-d";
        const string fileName = @"..\..\todos.txt";
        const string logFileName = @"..\..\log.txt";
        public MainWindow()
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));
            Todo.Logger = LogToMessage;
            Todo.Logger += LogToFile;
            LoadDataFromFile();
            lvTasks.ItemsSource = TaskList;
            lvTasks.Items.Refresh();
        }

        static void SaveDataToFile()
        {

            try
            {
                using (TextWriter tw = new StreamWriter(fileName, false))
                {
                    TaskList = TaskList.OrderBy(t => t.Task).ToList();
                    foreach (var t in TaskList)
                    {
                        tw.WriteLine(t.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public static void LogToMessage(string msg)
        {
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        public static void LogToFile(string msg)
        {
            try
            {
                File.AppendAllText(logFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + msg + "\n");
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error writing log file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        static void LoadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(fileName))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            TaskList.Add(new Todo(ln));
                        }
                        catch (InvalidValueExcception ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    {
                        Todo.Logger?.Invoke("Logger from load: " + String.Join("\n", errorsList));
                        MessageBox.Show(String.Join("\n", errorsList),"Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Error reading file: " + ex.Message,"Error",MessageBoxButton.OK , MessageBoxImage.Warning);
            }
        }


        private void CleanInputs()
        {
            tbTask.Text = "";
            sliderDiff.Value = 1;
            datePicker.ClearValue.
            //tbDueDate.Text = "";
            comboStatus.SelectedIndex = 0;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbTask.Text == "")
            {
                MessageBox.Show("Enter tne task");
                return;
            }
            //if (!DateTime.TryParseExact(tbDueDate.Text, DatePattern, null,
            //                       DateTimeStyles.None, out DateTime date))
            //{
            //    MessageBox.Show("Date format must be \"y-M-d\" (ex. 21-12-31)");
            //    return;
            //}
            DateTime datePick = datePicker.SelectedDate.GetValueOrDefault();
            string task = tbTask.Text;
            int diff = (int)sliderDiff.Value;
            Todo.StatusEnum status = (Todo.StatusEnum)comboStatus.SelectedItem;
            try
            {
                TaskList.Add(new Todo(task, diff, datePick, status));
            }
            catch (InvalidValueExcception ex)
            {
                Todo.Logger?.Invoke("Logger from add: " + ex.Message);
                MessageBox.Show(ex.Message);
                return;
            }
            lvTasks.ItemsSource = TaskList;
            lvTasks.Items.Refresh();
            CleanInputs();
        }

        private void lvTasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTasks.SelectedIndex != -1)
            {
                Todo t = (Todo)lvTasks.SelectedItem;
                tbTask.Text = t.Task;
                sliderDiff.Value = t.Difficulty;
                datePicker.SelectedDate = t.DueDate;
               // tbDueDate.Text = t.DueDate.ToString("y-M-d");
                comboStatus.SelectedItem = t.Status;
                btnDelete.IsEnabled = true;
                btnUpdate.IsEnabled = true;
            }
            else
            {
                CleanInputs();
                btnDelete.IsEnabled = false;
                btnUpdate.IsEnabled = false;
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvTasks.SelectedIndex != -1)
            {
                Todo t = (Todo)lvTasks.SelectedItem;
                if (tbTask.Text == "")
                {
                    MessageBox.Show("Enter tne task");
                    return;
                }
                //if (!DateTime.TryParseExact(tbDueDate.Text, DatePattern, null,
                //                   DateTimeStyles.None, out DateTime date))
                //{
                //    MessageBox.Show("Date format must be \"y-M-d\" (ex. 21-12-31)");
                //    return;
                //}
                try
                {
                    t.Task = tbTask.Text;
                    t.Difficulty = (int)sliderDiff.Value;
                    t.DueDate = datePicker.SelectedDate.GetValueOrDefault();
                    t.Status = (Todo.StatusEnum)comboStatus.SelectedItem;
                }
                catch (InvalidValueExcception ex)
                {
                    Todo.Logger?.Invoke("Logger from update: " + ex.Message);
                    MessageBox.Show(ex.Message);
                    return;
                }
                lvTasks.Items.Refresh();
                CleanInputs();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTasks.SelectedIndex != -1)
            {
                TaskList.RemoveAt(lvTasks.SelectedIndex);
                lvTasks.Items.Refresh();
                CleanInputs();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
            saveFileDialog.Title = "Export to file";
            string data = "";
            if (saveFileDialog.ShowDialog() == true)
            {
                foreach (Todo t in TaskList)
                {
                    data += t + "\n"; 
                }
                try
                {
                    File.WriteAllText(saveFileDialog.FileName, data);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing file: " + ex.Message);
                }
            }
        }
    }
}
