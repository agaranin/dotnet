﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Day05TodosInFile
{
	public class Todo
	{
        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate Logger;
        public enum StatusEnum { Pending, Done, Delegated }
        string _task;
		int _difficulty;
		DateTime _dueDate;
		StatusEnum _status;

        public Todo(string task, int difficulty, DateTime dueDate, StatusEnum status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }

        public Todo(string todo)
        {
            string[] data = todo.Split(';');
            if (data.Length != 4)
            {
                throw new InvalidValueExcception("Invalid number of fields");
            }
            if (!DateTime.TryParseExact(data[1], "y-M-d", CultureInfo.CurrentCulture,
                                   DateTimeStyles.None, out DateTime date))
            {
                throw new InvalidValueExcception("Wrong data structure. Cannot parse Due Date: " + data[1]);
            }
            if (!int.TryParse(data[2], out int diff))
            {
                throw new InvalidValueExcception("Wrong data structure. Cannot parse difficulty: " + data[2]);
            }
            if (!Enum.TryParse<StatusEnum>(data[3], out StatusEnum status))
            {
                throw new InvalidValueExcception("Wrong data structure. Cannot parse status: " + data[3]);
            }
            Task = data[0];
            DueDate = date;
            Difficulty = diff;
            Status = status;
        }



        public string Task
        {
            get => _task; set
            {
                if (!Regex.Match(value, @"\A[^;]{1,50}\Z").Success)
                {
                    Logger?.Invoke("Logger from setters: Task must not contain semicolons: " + value);
                    throw new InvalidValueExcception("Task must not contain semicolons: " + value);
                }
                _task = value;
            }
        }
        public int Difficulty
        {
            get => _difficulty; set
            {
                if (value < 1 || value > 5)
                {
                    throw new InvalidValueExcception("Difficulty must be between 1 and 5:  " + value);
                }
                _difficulty = value;
            }
        }
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }

            set
            {
                if ((value.CompareTo(new DateTime(1970, 01, 01)) < 0) || (value.CompareTo(new DateTime(2100, 01, 01)) > 0))
                {
                    throw new InvalidValueExcception($"Year must be between 1970 and 2100: {value:y-M-d}");
                }
                _dueDate = value;
            }
        }
        public StatusEnum Status
        {
            get => _status; set
            {
                _status = value;
            }
        }

     

        public override string ToString()
        {
            return $"{Task} by {DueDate.ToString("d", CultureInfo.CurrentCulture)} | diff. {Difficulty}, {Status}";
        }
        public string ToDataString()
        {
            return $"{Task};{DueDate:y-M-d};{Difficulty};{Status}";
        }
    }


}
