﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day07CarWithDialog
{
    public class Car
    {
        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other };
        string _makeModel;
        double _engineSize;
        FuelTypeEnum _fuelType;

        public Car(string makeModel, double engineSize, FuelTypeEnum fuelType)
        {
            MakeModel = makeModel;
            EngineSize = engineSize;
            FuelType = fuelType;
        }

        public Car(string carLine)
        {
            string[] data = carLine.Split(';');
            if (data.Length != 3)
            {
                throw new ArgumentException("Line structure is invalid");
            }
            MakeModel = data[0];

            if (!double.TryParse(data[1], out double engineSizeParsed))
            {
                throw new ArgumentException("Cannot parse Engine Size");
            }
            EngineSize = engineSizeParsed;
            if (!Enum.TryParse<FuelTypeEnum>(data[2], out FuelTypeEnum fuelTypeParsed))
            { 
                throw new ArgumentException("Cannot parse Fuel Type");
            }
            FuelType = fuelTypeParsed;
        }



        public string MakeModel
        {
            get => _makeModel; set
            {
                if (!Regex.Match(value, @"\A.{2,50}\Z").Success)
                {
                    throw new ArgumentException("Make model must be between 2 and 50 characters: " + value);
                }
                _makeModel = value;
            }
        }

        public double EngineSize
        {
            get => _engineSize; set
            {
                if (value > 20 || value < 0)
                {
                    throw new ArgumentException("Engine size must be between 2 and 50 characters: " + value);
                }
                _engineSize = value;
            }
        }

        public FuelTypeEnum FuelType { get => _fuelType; set => _fuelType = value; }

        public string ToDataString()
        {
            return $"{MakeModel};{EngineSize};{FuelType}";
        }
    }
}
