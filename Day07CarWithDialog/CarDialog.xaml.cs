﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07CarWithDialog
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        Car _car;
        public event Action<Car> AssignResult;
        public CarDialog(Car car)
        {
            InitializeComponent();
            comboFuelType.ItemsSource = Enum.GetValues(typeof (Car.FuelTypeEnum));
            comboFuelType.SelectedIndex = 0;

            if (car != null) // Double click to update
            {
                _car = car;
                tbMakeModel.Text = car.MakeModel;
                sliderEngineSize.Value = car.EngineSize;
                comboFuelType.SelectedItem = car.FuelType;
                btnSaveUpdate.Content = "Update";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string makeModel = tbMakeModel.Text;
            double engineSize = sliderEngineSize.Value;
            Car.FuelTypeEnum fuelType  = (Car.FuelTypeEnum)comboFuelType.SelectedItem;
            
            if (_car != null) // Double click to update
            {
                _car.MakeModel = makeModel;
                _car.EngineSize = engineSize;
                _car.FuelType = fuelType;
            }
            try
            {
                AssignResult?.Invoke(new Car(makeModel, engineSize, fuelType));
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DialogResult = true;
        }
    }
}
