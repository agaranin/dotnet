﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public event Action<string, string, string> AssignResult;
        public CustomDialog()
        {
            InitializeComponent();
            List<string> breadList = new List<string>();
            breadList.Add("White");
            breadList.Add("Whole wheat");
            breadList.Add("Sesame");
            comboBread.ItemsSource = breadList;
            comboBread.SelectedIndex = 0;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            string bread = (string)comboBread.SelectedItem;
            var veggArr = new List<string>();
            if (chkLettuce.IsChecked.Value) veggArr.Add((string)chkLettuce.Content);
            if (chkTomatoes.IsChecked.Value) veggArr.Add((string)chkTomatoes.Content);
            if (chkCucumber.IsChecked.Value) veggArr.Add((string)chkCucumber.Content);
            string veggies = String.Join(", ", veggArr);
            String meat = "";
            if (radioChicken.IsChecked == true) meat = (string)radioChicken.Content;
            if (radioTurkey.IsChecked == true) meat = (string)radioTurkey.Content;
            if (radioTofu.IsChecked == true) meat = (string)radioTofu.Content;
            AssignResult?.Invoke(bread, veggies, meat);
            DialogResult = true; // also closes the dialog
        }
    }
}
