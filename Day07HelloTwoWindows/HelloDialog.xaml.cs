﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for HelloDialog.xaml
    /// </summary>
    public partial class HelloDialog : Window
    {
        public event Action<int> ResultAge;
        public HelloDialog(string name)
        {
            InitializeComponent();
            lblMessage.Content = string.Format($"Hello {name}, nice to meet you");
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            int age = int.Parse(tbAge.Text); //FIXME: ex
            ResultAge?.Invoke(age);
            DialogResult = true; // also closes the dialog
        }
    }
}
