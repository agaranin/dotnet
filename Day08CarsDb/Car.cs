﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day08CarsDb
{
    public class Car
    {
        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other };
        int _id;
        string _makeModel;
        double _engineSize;
        FuelTypeEnum _fuelType;

        public Car(int id, string makeModel, double engineSize, FuelTypeEnum fuelType)
        {
            Id = id;
            MakeModel = makeModel;
            EngineSize = engineSize;
            FuelType = fuelType;
        }

        public Car(SqlDataReader reader) // ex SQL and Argument
        {
            Id = reader.GetInt32(reader.GetOrdinal("Id"));
            MakeModel = reader.GetString(reader.GetOrdinal("MakeModel"));
            EngineSize = (double)reader.GetFloat(reader.GetOrdinal("EngineSize"));
            string fuelTypeStr = reader.GetString(reader.GetOrdinal("FuelType"));
            if (!Enum.TryParse<FuelTypeEnum>(fuelTypeStr, out FuelTypeEnum fuelTypeParsed))
            {
                throw new ArgumentException("Cannot parse Fuel Type");
            }
            FuelType = fuelTypeParsed;
        }


        public string MakeModel
        {
            get => _makeModel; set
            {
                if (!Regex.Match(value, @"\A.{2,50}\Z").Success)
                {
                    throw new ArgumentException("Make model must be between 2 and 50 characters: " + value);
                }
                _makeModel = value;
            }
        }

        public double EngineSize
        {
            get => Math.Round(_engineSize,1); set
            {
                if (value > 20 || value < 0)
                {
                    throw new ArgumentException("Engine size must be between 2 and 50 characters: " + value);
                }
                _engineSize = value;
            }
        }

        public FuelTypeEnum FuelType { get => _fuelType; set => _fuelType = value; }
        public int Id { get => _id; set => _id = value; }

        public string ToDataString()
        {
            return $"{MakeModel};{EngineSize};{FuelType}";
        }
    }
}
