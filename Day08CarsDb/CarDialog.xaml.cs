﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day08CarsDb
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        Car currCar;
        public CarDialog(Car car)
        {
            InitializeComponent();
            comboFuelType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum));
            comboFuelType.SelectedIndex = 0;

            if (car != null) // update car
            {
                currCar = car;
                tbMakeModel.Text = car.MakeModel;
                sliderEngineSize.Value = car.EngineSize;
                comboFuelType.SelectedItem = car.FuelType;
                btnSaveUpdate.Content = "Update";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string makeModel = tbMakeModel.Text;
                double engineSize = sliderEngineSize.Value;
                Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum)comboFuelType.SelectedItem;
                if (currCar != null) // update car
                {
                    Database.UpdateCar(new Car(currCar.Id, makeModel, engineSize, fuelType));
                }
                else
                {
                    Database.AddCar(new Car(0, makeModel, engineSize, fuelType));
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DialogResult = true;
        }
    }
}
