﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day08CarsDb
{
    class Database
    {
        const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Artem\Documents\dotnet\Day08CarsDb\CarsDb.mdf;Integrated Security=True;Connect Timeout=30";
        private static SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(ConnString);
            conn.Open(); //ex
        }

        public List<Car> GetAllCars() //ex
        {
                List<Car> list = new List<Car>();
                using (SqlCommand cmd = new SqlCommand("SELECT Id, MakeModel, EngineSize, FuelType FROM Cars", conn))
                using (SqlDataReader reader = cmd.ExecuteReader()) //ex
                {
                    while (reader.Read())
                    {
                        list.Add(new Car(reader)); //ex
                    }
                }
                return list;
        }

        public static void AddCar(Car car)
        {
            using (SqlCommand cmd = new SqlCommand("INSERT INTO cars (MakeModel, EngineSize, FuelType) VALUES (@MakeModel, @EngineSize, @FuelType)", conn))
            {
                cmd.Parameters.AddWithValue("@MakeModel", car.MakeModel);
                cmd.Parameters.AddWithValue("@EngineSize", car.EngineSize);
                cmd.Parameters.AddWithValue("@FuelType", Enum.GetName(typeof(Car.FuelTypeEnum), car.FuelType));
                cmd.ExecuteNonQuery(); // ex
            }

        }
        public static void UpdateCar(Car car)
        {
            using (SqlCommand cmd = new SqlCommand("UPDATE cars SET MakeModel=@MakeModel, EngineSize=@EngineSize, FuelType=@FuelType WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@MakeModel", car.MakeModel);
                cmd.Parameters.AddWithValue("@EngineSize", car.EngineSize);
                cmd.Parameters.AddWithValue("@FuelType", Enum.GetName(typeof(Car.FuelTypeEnum), car.FuelType));
                cmd.Parameters.AddWithValue("@Id", car.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }
        public void DeleteCar(Car car)
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM cars WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Id", car.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }


    }
}
