﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08CarsDb
{
    public partial class MainWindow : Window
    {
        Database db;
        static List<Car> CarList = new List<Car>();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                db = new Database();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error connecting to the Database" + ex.Message, "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
            ((INotifyCollectionChanged)lvCars.Items).CollectionChanged += lvCars_CollectionChanged;
            GetDataFromDatabase();
        }
        private void lvCars_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            tbStatus.Text = $"You have {CarList.Count} car(s) currently";
        }

        public void GetDataFromDatabase()
        {
            try
            {
                CarList = db.GetAllCars();
                lvCars.ItemsSource = CarList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void EditItemCurrSelected()
        {
            Car car = (Car)lvCars.SelectedItem;
            if (car == null) { return; }
            CarDialog carDialog = new CarDialog(car) { Owner = this };
            carDialog.ShowDialog();
            GetDataFromDatabase();
        }

        private void MenuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            CarDialog carDialog = new CarDialog(null) { Owner = this };
            carDialog.ShowDialog();

        }

        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItemCurrSelected();
        }

        private void MenuItemExportToCsv_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV file (*.csv)|*.csv";
            saveFileDialog.Title = "Export to file";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.Delimiter = ";";
                        csv.WriteRecords(CarList);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                MessageBox.Show("Select item to delete", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                db.DeleteCar((Car)lvCars.SelectedItem);
                GetDataFromDatabase();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void MenuItemEdit_Click(object sender, RoutedEventArgs e)
        {
            EditItemCurrSelected();
        }
    }
}
