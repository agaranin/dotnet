﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day08SimpleDatabase
{
    class Program
    {
            const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Artem\Documents\dotnet\Day08SimpleDatabase\SimpleDb.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConnString);
                conn.Open(); //ex
                Random random = new Random();

                {//insert
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO People (Name, Age) VALUES (@Name, @Age)", conn))
                    {
                        cmd.Parameters.AddWithValue("@Name", "Jerry" + random.Next());
                        cmd.Parameters.AddWithValue("@Age", random.Next(1, 100));
                        cmd.ExecuteNonQuery(); // ex
                    }
                }
                {//select
                    using (SqlCommand cmd = new SqlCommand("SELECT * from People", conn))
                    using (SqlDataReader reader = cmd.ExecuteReader()) //ex
                    {
                        while (reader.Read())
                        {
                            //int id = (int)reader["Id"];
                            //int id = reader.GetInt32(0);
                            int id = reader.GetInt32(reader.GetOrdinal("Id"));
                            string name = reader.GetString(reader.GetOrdinal("Name"));
                            int age = reader.GetInt32(reader.GetOrdinal("Age"));
                            Console.WriteLine($"{id}, {name} is {age} y/o");
                        }
                    }
                }

                {//select specific record
                    int wantId = 3;
                    Console.WriteLine("Looking for record with Id=" + wantId); ;
                    using (SqlCommand cmd = new SqlCommand("SELECT * from People where Id=@Id", conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", wantId);
                        using (SqlDataReader reader = cmd.ExecuteReader()) //ex
                        {
                            while (reader.Read())
                            {
                                //int id = (int)reader["Id"];
                                //int id = reader.GetInt32(0);
                                int id = reader.GetInt32(reader.GetOrdinal("Id"));
                                string name = reader.GetString(reader.GetOrdinal("Name"));
                                int age = reader.GetInt32(reader.GetOrdinal("Age"));
                                Console.WriteLine($"{id}, {name} is {age} y/o");
                            }
                        }
                    }
                }
            }
            finally catch (SqlException ex)
                { 
            
            }
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}
