﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day08TodoDb
{
    /// <summary>
    /// Interaction logic for AddEditTodo.xaml
    /// </summary>
    public partial class AddEditTodo : Window
    {
        Todo currTodo;
        public AddEditTodo(Todo todo)
        {
            InitializeComponent();
            if (todo != null) // update task
            {
                currTodo = todo;
                lblId.Content = todo.Id;
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                if (todo.Status == Todo.TaskStatusEnum.Done)
                {
                    cbIsDone.IsChecked = true;
                }
                btnSave.Content = "Update";
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                DateTime dateDue = dpDueDate.SelectedDate.GetValueOrDefault();
                Todo.TaskStatusEnum status;
                status = cbIsDone.IsChecked == true? status = Todo.TaskStatusEnum.Done: Todo.TaskStatusEnum.Pending;
                if (currTodo != null) // update car
                {
                    Database.UpdateTask(new Todo(currTodo.Id, task, dateDue, status));
                    MainWindow.StatusStr = $"Task with ID = {currTodo.Id} has beed modified.";
                }
                else
                {
                    int id = Database.AddTask(new Todo(0, task, dateDue, status));
                    MainWindow.StatusStr = $"A new task with ID = {id} has beed added.";
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DialogResult = true;
        }
    }
}
