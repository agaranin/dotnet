﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day08TodoDb
{
    class Database
    {
        const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Artem\Documents\dotnet\Day08TodoDb\TodoDb.mdf;Integrated Security=True;Connect Timeout=30";
        private static SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(ConnString);
            conn.Open(); //ex
        }

        public List<Todo> GetAllTasks() //ex
        {
            List<Todo> list = new List<Todo>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Tasks", conn))
            using (SqlDataReader reader = cmd.ExecuteReader()) //ex
            {
                while (reader.Read())
                {
                    list.Add(new Todo(reader)); //ex
                }
            }
            return list;
        }

        public static int AddTask(Todo todo)
        {
            using (SqlCommand cmd = new SqlCommand("INSERT INTO Tasks (Task, DueDate, Status) VALUES (@Task, @DueDate, @Status) SELECT SCOPE_IDENTITY()", conn))
            {
                cmd.Parameters.AddWithValue("@Task", todo.Task);
                cmd.Parameters.AddWithValue("@DueDate", todo.DueDate);
                cmd.Parameters.AddWithValue("@Status", Enum.GetName(typeof(Todo.TaskStatusEnum), todo.Status));
                return Convert.ToInt32(cmd.ExecuteScalar());
            }

        }
        public static void UpdateTask(Todo todo)
        {
            using (SqlCommand cmd = new SqlCommand("UPDATE Tasks SET Task=@Task, DueDate=@DueDate, Status=@Status WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Task", todo.Task);
                cmd.Parameters.AddWithValue("@DueDate", todo.DueDate);
                cmd.Parameters.AddWithValue("@Status", Enum.GetName(typeof(Todo.TaskStatusEnum), todo.Status));
                cmd.Parameters.AddWithValue("@Id", todo.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }
        public void DeleteTask(Todo todo)
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Tasks WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Id", todo.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }
    }
}
