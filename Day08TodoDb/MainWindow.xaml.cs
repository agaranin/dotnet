﻿using CsvHelper;
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TodoDb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        static List<Todo> TodoList = new List<Todo>();
        public static string StatusStr;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                db = new Database();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error connecting to the Database" + ex.Message, "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
            ((INotifyCollectionChanged)lvTasks.Items).CollectionChanged += lvTasks_CollectionChanged;
            GetDataFromDatabase();
            tbStatus.Text = $"You have {TodoList.Count} task(s) currently";
        }
        private void lvTasks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

             tbStatus.Text = StatusStr;
        }

        public void GetDataFromDatabase()
        {
            try
            {
                TodoList = db.GetAllTasks();
                lvTasks.ItemsSource = TodoList;
                Sort();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void EditItemCurrSelected()
        {
            Todo todo = (Todo)lvTasks.SelectedItem;
            if (todo == null) { return; }
            AddEditTodo addEditTodo = new AddEditTodo(todo) { Owner = this };
            addEditTodo.ShowDialog();
            GetDataFromDatabase();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditTodo carDialog = new AddEditTodo(null) { Owner = this };
            carDialog.ShowDialog();
            GetDataFromDatabase();
        }

        private void lvTasks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItemCurrSelected();
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTasks.SelectedIndex == -1)
            {
                MessageBox.Show("Select item to delete", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                Todo todo = (Todo)lvTasks.SelectedItem;
                db.DeleteTask(todo);
                StatusStr = $"Task with ID = {todo.Id} has beed deleted.";
                GetDataFromDatabase();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void MenuItemEdit_Click(object sender, RoutedEventArgs e)
        {
            EditItemCurrSelected();
        }

        public void Sort()
        {
            TodoList = rbSortTask.IsChecked == true ?
            TodoList.OrderBy(t => t.Task).ToList() :
            TodoList.OrderBy(t => t.DueDate).ToList();
            lvTasks.ItemsSource = TodoList;
        }

        private void rbSort_Click(object sender, RoutedEventArgs e)
        {
            Sort();
        }

        private void MenuItemExportToCSV_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV file (*.csv)|*.csv";
            saveFileDialog.Title = "Export to file";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        var options = new TypeConverterOptions { Formats = new[] { "d" } };
                        csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                        csv.Configuration.Delimiter = ";";
                        csv.WriteRecords(TodoList);
                        tbStatus.Text = $"Records exported successfully.";
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
