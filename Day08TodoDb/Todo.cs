﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day08TodoDb
{
    public class Todo
    {
        public enum TaskStatusEnum {Done, Pending};
        int _id;
        string _task;
        DateTime _dueDate;
        TaskStatusEnum _status;

        public Todo(int id, string task, DateTime dueDate, TaskStatusEnum status)
        {
            Id = id;
            Task = task;
            DueDate = dueDate;
            Status = status;
        }

        public Todo(SqlDataReader reader)
        {
            Id = reader.GetInt32(reader.GetOrdinal("Id"));
            Task = reader.GetString(reader.GetOrdinal("Task"));
            DueDate = reader.GetDateTime(reader.GetOrdinal("DueDate"));
            string fuelTypeStr = reader.GetString(reader.GetOrdinal("Status"));
            if (!Enum.TryParse<TaskStatusEnum>(fuelTypeStr, out TaskStatusEnum statusParsed))
            {
                throw new ArgumentException("Cannot parse Fuel Type");
            }
            Status = statusParsed;
        }


        public int Id
        {
            get => _id; set
            {
                _id = value;
            }
        }
        public string Task
        {
            get => _task; set
            {
                if (!Regex.Match(value, @"\A.{1,50}\Z").Success)
                {
                    throw new ArgumentException("Task must be between 1 and 50 characters: " + value);
                }
                _task = value;
            }
        }
        public DateTime DueDate
        {
            get => _dueDate; set
            {
                _dueDate = value;
            }
        }

        public TaskStatusEnum Status { get => _status; set => _status = value; }
    }
}
