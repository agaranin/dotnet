﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SocietyDbContext ctx = new SocietyDbContext();
                Random random = new Random();

                // equivalent of insert
                Person p1 = new Person() { Name = "Jerry", Age = random.Next(100) };

                ctx.People.Add(p1); // insert operation, NOT executed yet
                ctx.SaveChanges(); // synchronize objects in memory with database
                Console.WriteLine("record added");

                // eq. of update - fetch then modify, save changes
                Person p2 = (from p in ctx.People where p.Id == 2 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {
                    p2.Name = "Alabama" + (random.Next(10000) + 10000);
                    ctx.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Record not found");
                }

                // delete - fetch then deletion then changes
                Person p3 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
                if (p3 != null)
                { // found record
                    ctx.People.Remove(p3);
                    ctx.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Record not found");
                }

                // fetch all records
                List<Person> peopleList = (from p in ctx.People select p).ToList<Person>();
                foreach (Person p in peopleList)
                {
                    Console.WriteLine($"{p.Id}: {p.Name} is {p.Age} y/o");
                }
            }
            catch (SystemException ex) //catch all for EF, SQL and many other exception
            {
                Console.WriteLine("Database operation failed: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
           
        }
    }
}
