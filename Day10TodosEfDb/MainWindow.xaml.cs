﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10TodosEfDb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TasksDbContext ctx;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                ctx = new TasksDbContext();
                comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));
                comboStatus.SelectedIndex = 0;
                FetchRecords();
            }
            catch (SystemException ex) //catch all for EF, SQL and many other exception
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearInputs()
        {
            tbTask.Text = "";
            sliderDiff.Value = 1;
            datePicker.SelectedDate = DateTime.Now;
            comboStatus.SelectedIndex = 0;
            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
        }

        public void FetchRecords()
        {
            try
            {
                lvTasks.ItemsSource = (from todo in ctx.Tasks select todo).ToList<Todo>();
                ResizeGridViewColumn(GridViewColumnTask);
            }
            catch (SystemException ex) //catch all for EF, SQL and many other exception
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ResizeGridViewColumn(GridViewColumn column)
        {
            if (double.IsNaN(column.Width))
            {
                column.Width = column.ActualWidth;
            }
            column.Width = double.NaN;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Todo t = new Todo
                {
                    Task = tbTask.Text,
                    Difficulty = (int)sliderDiff.Value,
                    DueDate = datePicker.SelectedDate.GetValueOrDefault(),
                    Status = (Todo.StatusEnum)comboStatus.SelectedItem
                };
                ctx.Tasks.Add(t);
                ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void lvTasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTasks.SelectedIndex == -1)
            {
                ClearInputs();
                return;
            }
            Todo t = (Todo)lvTasks.SelectedItem;
            tbTask.Text = t.Task;
            sliderDiff.Value = t.Difficulty;
            datePicker.SelectedDate = t.DueDate;
            comboStatus.SelectedItem = t.Status;
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Todo todoCurr = (Todo)lvTasks.SelectedItem;
            if (todoCurr == null) { return; }
            try
            {
                todoCurr = (from t in ctx.Tasks where t.Id == todoCurr.Id select t).FirstOrDefault<Todo>();
                if (todoCurr == null)
                {
                    MessageBox.Show("Record not found", "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                todoCurr.Task = tbTask.Text;
                todoCurr.Difficulty = (int)sliderDiff.Value;
                todoCurr.DueDate = datePicker.SelectedDate.GetValueOrDefault();
                todoCurr.Status = (Todo.StatusEnum)comboStatus.SelectedItem;
                ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Todo todoCurr = (Todo)lvTasks.SelectedItem;
            if (todoCurr == null) { return; }
            if (MessageBoxResult.No == MessageBox.Show("Do you want to delete the record?\n" + todoCurr, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                todoCurr = (from t in ctx.Tasks where t.Id == todoCurr.Id select t).FirstOrDefault<Todo>();
                if (todoCurr == null)
                {
                    MessageBox.Show("Record not found", "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                ctx.Tasks.Remove(todoCurr);
                ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}
