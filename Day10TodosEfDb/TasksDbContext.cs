﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10TodosEfDb
{
    class TasksDbContext : DbContext
    {
        //public TasksDbContext() : base("Day10TasksDb") { }
        public TasksDbContext() : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Artem\Documents\dotnet\Day10TodosEfDb\Day10TasksDb.mdf;Integrated Security=True;Connect Timeout=30") { }
        public virtual DbSet<Todo> Tasks { get; set; }
    }
}
