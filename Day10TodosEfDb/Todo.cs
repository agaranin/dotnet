﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace Day10TodosEfDb
{
    [Table("Tasks")]
    public class Todo
    {
        public enum StatusEnum { Pending, Done, Delegated }
        string _task;
        public int Id { get; set; }
        [Required] // means not-null
        [StringLength(100)] // nvarchar(100)
        public string Task
        {
            get => _task; set
            {
                if (!Regex.Match(value, @"\A.{1,100}\Z").Success)
                {
                    throw new ArgumentException("Task must be between 1 and 100: " + value);
                }
                _task = value;
            }
        }
        public int Difficulty { get; set; }
        public DateTime DueDate { get; set; }
        [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }

        public override string ToString()
        {
            return $"ID={Id}, {Task} by {DueDate:d}, diff. {Difficulty}, {Status}";
        }
    }
}