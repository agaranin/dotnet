﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        Owner currOwner;
        internal CarsDialog(Owner owner)
        {
            InitializeComponent();
            currOwner = owner;
            lblDialogName.Content = currOwner.Name;
            FetchCarRecords();
        }

        public void FetchCarRecords()
        {
            try
            {
                // we don't necessarily need to run a query here
                List<Car> carList = currOwner.CarsInGarage.ToList<Car>();
                lvDialogCars.ItemsSource = carList;
                Utils.AutoResizeColumns(lvDialogCars);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearDialogInputs()
        {
            tbDialogMakeModel.Text = "";
            lblDialogId.Content = "";
            btnDialogDelete.IsEnabled = false;
            btnDialogUpdate.IsEnabled = false;
        }

        public bool IsDialogFieldsValid()
        {
            if (tbDialogMakeModel.Text.Length < 1)
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btnDialogDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnDialogAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            try
            {
                Car c = new Car
                {
                    MakeModel = tbDialogMakeModel.Text,
                    OwnerId = currOwner.Id
                };
                Globals.ctx.Cars.Add(c);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                FetchCarRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void lvDialogCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDialogCars.SelectedIndex == -1)
            {
                ClearDialogInputs();
                return;
            }
            Car c = (Car)lvDialogCars.SelectedItem;
            lblDialogId.Content = c.Id;
            tbDialogMakeModel.Text = c.MakeModel;
            btnDialogDelete.IsEnabled = true;
            btnDialogUpdate.IsEnabled = true;
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            Car carCurr = (Car)lvDialogCars.SelectedItem;
            if (carCurr == null) { return; }
            try
            {
                carCurr.MakeModel = tbDialogMakeModel.Text;
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                FetchCarRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            Car carCurr = (Car)lvDialogCars.SelectedItem;
            if (carCurr == null) { return; }
            if (MessageBoxResult.No == MessageBox.Show("Do you want to delete the record?\n" + carCurr, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx.Cars.Remove(carCurr);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                FetchCarRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}
