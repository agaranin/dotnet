﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        byte[] currOwnerImage;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new CarsOwnersDbContext();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal error
            }
        }
        
        public void ClearInputs()
        {
            tbName.Text = "";
            imageViewer.Source = null;
            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            btnManageCars.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }

        public void FetchRecords()
        {
            try
            { // Include means to force eager loading - used for collections in OneToMany relations
                lvOwners.ItemsSource = Globals.ctx.Owners.Include("CarsInGarage").ToList();
                // ResizeGridViewColumn(GridViewColumnName);
                Utils.AutoResizeColumns(lvOwners);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ResizeGridViewColumn(GridViewColumn column)
        {
            if (double.IsNaN(column.Width))
            {
                column.Width = column.ActualWidth;
            }
            column.Width = double.NaN;
        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName);
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public bool IsFieldsValid()
        {
            if (tbName.Text.Length < 2)
            {
                MessageBox.Show("Name must be between 2 and 100 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (currOwnerImage == null)
            {
                MessageBox.Show("Choose a picture", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Owner o = new Owner
                {
                    Name = tbName.Text,
                    Photo = currOwnerImage,
                };
                Globals.ctx.Owners.Add(o);
                Globals.ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnManageCars_Click(object sender, RoutedEventArgs e)
        {
            Owner owner = (Owner)lvOwners.SelectedItem;
            CarsDialog carDialog = new CarsDialog(owner) { Owner = this };
            carDialog.ShowDialog();
            FetchRecords();
        }

        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwners.SelectedIndex == -1)
            {
                ClearInputs();
                return;
            }
            try { 
                Owner o = (Owner)lvOwners.SelectedItem;
                lblOwnerId.Content = o.Id;
                tbName.Text = o.Name;
                currOwnerImage = o.Photo;
                imageViewer.Source = Utils.GetBitmapImageV2(o.Photo); // ex
                btnDelete.IsEnabled = true;
                btnUpdate.IsEnabled = true;
                btnManageCars.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            Owner ownerCurr = (Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; }
            try
            {
                ownerCurr.Name = tbName.Text;
                ownerCurr.Photo = currOwnerImage;
                Globals.ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Owner ownerCurr = (Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; }
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n" + ownerCurr, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Globals.ctx.Owners.Remove(ownerCurr);
                Globals.ctx.SaveChanges();
                ClearInputs();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
