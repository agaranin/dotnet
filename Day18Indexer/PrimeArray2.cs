﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day18Indexer
{
    class PrimeArray2
    {
        public static bool IsPrime(long num)
        {
            if (num <= 1) return false;
            if (num == 2) return true;
            if (num % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(num));

            for (int i = 3; i <= boundary; i += 2)
                if (num % i == 0)
                    return false;

            return true;
        }
        public long this[int nth]
        {
            get
            {
                long num = 1;
                int count = 0;
                while (true)
                {
                    num++;
                    if (IsPrime(num))
                    {
                        count++;
                    }
                    if (count == nth)
                    {
                        return num;
                    }
                }
            }
        }
    }
}
