﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day18Indexer
{
    class Program
    {
        static void Main(string[] args)
        {
            // Part 1
            PrimeArray pa = new PrimeArray();
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine(i + " : " + pa[i]);
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();
            Console.WriteLine();
           //Part 2
           PrimeArray2 pa2 = new PrimeArray2();
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine(i + " : " + pa2[i]);
            }
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
