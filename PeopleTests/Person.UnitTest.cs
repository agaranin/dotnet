﻿using Day01PeopleListInFile;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PeopleTests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void GettersReturnValuesPovidedToTheConstructor()
        {
            Person person = new Person(30, "Montreal", "Artem");
            Assert.AreEqual<int>(30, person.Age);
            Assert.AreEqual<string>("Montreal", person.City);
            Assert.AreEqual<string>("Artem", person.Name);
        }
        [TestMethod]
        public void GetterAgeReturns_30()
        {
            Person person = new Person();
            person.Age = 30;
            Assert.AreEqual(30, person.Age);
        }
        [TestMethod]
        public void GetterCityReturns_Montreal()
        {
            Person person = new Person();
            person.City = "Montreal";
            Assert.AreEqual("Montreal", person.City);
        }
        [TestMethod]
        public void GetterNameReturns_Artem()
        {
            Person person = new Person();
            person.Name = "Artem";
            Assert.AreEqual("Artem", person.Name);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeAge_ThrowsArgumentException()
        {
            Person person = new Person();
            person.Age = -1;
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidAge_ThrowsArgumentException()
        {
            Person person = new Person();
            person.Age = 151;
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        public void AgeValidation_ValidData()
        {
            Person person = new Person();
            Assert.AreEqual(person.Age = 0, person.Age, "Age validation error. Age = 0 ");
            Assert.AreEqual(person.Age = 30, person.Age, "Age validation error. Age = 30 ");
            Assert.AreEqual(person.Age = 150, person.Age, "Age validation error. Age = 150 ");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CityValidation_LengthLessThan_2_ThrowsArgumentException()
        {
            Person person = new Person();
            person.City = "M";
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CityValidation_LengthMoreThan_100_ThrowsArgumentException()
        {
            Person person = new Person();
            person.City = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901";
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CityValidation_ContainingSemicolon_ThrowsArgumentException()
        {
            Person person = new Person();
            person.City = "Montreal;";
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NameValidation_LengthLessThan_2_ThrowsArgumentException()
        {
            Person person = new Person();
            person.Name = "A";
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NameValidation_LengthMoreThan_100_ThrowsArgumentException()
        {
            Person person = new Person();
            person.Name = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901";
            Assert.Fail("Exception must happen");
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NameValidation_ContainingSemicolon_ThrowsArgumentException()
        {
            Person person = new Person();
            person.Name = "Artem;";
            Assert.Fail("Exception must happen");
        }

        [TestMethod]
        public void ToDataStringReturnsCommaSeparatedValues()
        {
            Person person = new Person(30, "Montreal", "Artem");
            Assert.AreEqual(person.ToDataString(), "Artem,30,Montreal");
        }
        [TestMethod]
        public void ToStringReturnsRightString()
        {
            Person person = new Person(30, "Montreal", "Artem");
            Assert.AreEqual(person.ToString(), "Artem is 30 from Montreal");
        }
    }
}
