﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QUIZ_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Trip> TripList = new List<Trip>();
        const string fileName = @"..\..\trips.txt";
        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromFile();
            lvTrips.ItemsSource = TripList;
            lvTrips.Items.Refresh();
        }

        static void SaveDataToFile()
        {

            try
            {
                using (TextWriter tw = new StreamWriter(fileName, false))
                {
                    foreach (var t in TripList)
                    {
                        tw.WriteLine(t.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        static void LoadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(fileName))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            TripList.Add(new Trip(ln));
                        }
                        catch (DataInvalidException ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    {
                        MessageBox.Show(String.Join("\n", errorsList), "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Error reading file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private void ClearInputs()
        {
            tbDest.Text = "";
            tbName.Text = "";
            tbPassport.Text = "";
            dpDepart.Text = "";
            dpReturn.Text = "";
        }

        private void btnAddTrip_Click(object sender, RoutedEventArgs e)
        {
            if (tbDest.Text == "" || tbName.Text == "" || tbPassport.Text == "" || dpDepart.SelectedDate == null || dpReturn.SelectedDate == null)
            {
                MessageBox.Show("Fill all the fields", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            string dest = tbDest.Text;
            string name = tbName.Text;
            string passport = tbPassport.Text;
            DateTime dateDep = dpDepart.SelectedDate.Value;
            DateTime dateRet = dpReturn.SelectedDate.Value;
            try
            {
                TripList.Add(new Trip(dest, name, passport, dateDep, dateRet));
            }
            catch (DataInvalidException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            lvTrips.ItemsSource = TripList;
            lvTrips.Items.Refresh();
            ClearInputs();
        }

        private void lvTrips_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTrips.SelectedIndex != -1)
            {
                Trip t = (Trip)lvTrips.SelectedItem;
                tbDest.Text = t.Destination;
                tbName.Text = t.TravellerName;
                tbPassport.Text = t.TravellerPassport;
                dpDepart.SelectedDate = t.DepartureDate;
                dpReturn.SelectedDate = t.ReturnDate;
                btnDeleteTrip.IsEnabled = true;
                btnEditTrip.IsEnabled = true;
            }
            else
            {
                ClearInputs();
                btnDeleteTrip.IsEnabled = false;
                btnEditTrip.IsEnabled = false;
            }
        }

        private void btnEditTrip_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrips.SelectedIndex != -1)
            {
                Trip t = (Trip)lvTrips.SelectedItem;
                if (tbDest.Text == "" || tbName.Text == "" || tbPassport.Text == "" || dpDepart.SelectedDate == null || dpReturn.SelectedDate == null)
                {
                    MessageBox.Show("Fill all the fields", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                try
                {
                    t.Destination = tbDest.Text;
                    t.TravellerName = tbName.Text;
                    t.TravellerPassport = tbPassport.Text;
                    t.DepartureDate = dpDepart.SelectedDate.Value;
                    t.ReturnDate = dpReturn.SelectedDate.Value;
                }
                catch (DataInvalidException ex)
                {
                    MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                lvTrips.Items.Refresh();
                ClearInputs();
            }
        }

        private void btnDeleteTrip_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrips.SelectedIndex != -1)
            {
                TripList.RemoveAt(lvTrips.SelectedIndex);
                lvTrips.Items.Refresh();
                ClearInputs();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile();
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrips.SelectedIndex != -1)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Trips file (*.trips)|*.trips";
                saveFileDialog.Title = "Export to file";
                string data = "";
                if (saveFileDialog.ShowDialog() == true)
                {
                    foreach (Trip t in lvTrips.SelectedItems)
                    {
                        data += t.ToDataString() + "\n";
                    }
                    try
                    {
                        File.WriteAllText(saveFileDialog.FileName, data);
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            else
            {
                MessageBox.Show("Select lines to export.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
