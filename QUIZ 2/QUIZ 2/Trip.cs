﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QUIZ_2
{
    class Trip
    {
        string _destination;
        string _travellerName;
        string _travellerPassport;
        DateTime _departureDate;
        DateTime _returnDate;

        public Trip(string destination, string travellerName, string travellerPassport, DateTime departureDate, DateTime returnDate)
        {
            Destination = destination;
            TravellerName = travellerName;
            TravellerPassport = travellerPassport;
            DepartureDate = departureDate;
            ReturnDate = returnDate;
        }

        public Trip(string trip)
        {
            string[] data = trip.Split(';');
            if (data.Length != 5)
            {
                throw new DataInvalidException("Invalid number of fields");
            }
            if (!DateTime.TryParseExact(data[3], "MM/dd/yyyy", CultureInfo.CurrentCulture,
                                   DateTimeStyles.None, out DateTime depDate))
            {
                throw new DataInvalidException("Wrong data structure. Cannot parse Departure Date: " + data[3]);
            }
            if (!DateTime.TryParseExact(data[4], "MM/dd/yyyy", CultureInfo.CurrentCulture,
                                   DateTimeStyles.None, out DateTime retDate))
            {
                throw new DataInvalidException("Wrong data structure. Cannot parse Departure Date: " + data[4]);
            }
            Destination = data[0];
            TravellerName = data[1];
            TravellerPassport = data[2];
            DepartureDate = depDate;
            ReturnDate = retDate;
        }
        public string ToDataString()
        {
            return $"{Destination};{TravellerName};{TravellerPassport};{DepartureDate:MM/dd/yyyy};{ReturnDate:MM/dd/yyyy}";
        }

        public string Destination
        {
            get => _destination; set
            {
                if (!Regex.Match(value, @"\A[^;]{2,30}\Z").Success)
                {
                    throw new DataInvalidException("Destination must be between 2 and 30 characters, no semicolon: " + value);
                }
                _destination = value;
            }
        }
        public string TravellerName
        {
            get => _travellerName; set
            {
                if (!Regex.Match(value, @"\A[^;]{2,30}\Z").Success)
                {
                    throw new DataInvalidException("Traveller name must be between 2 and 30 characters, no semicolon: " + value);
                }
                _travellerName = value;
            }
        }
        public string TravellerPassport
        {
            get => _travellerPassport; set
            {
                if (!Regex.Match(value, @"\A[A-Z]{2}[0-9]{6}\Z").Success)
                {
                    throw new DataInvalidException("Traveller Passport must be \"AA123456\" frormat : " + value);
                }
                _travellerPassport = value;
            }
        }
        public DateTime DepartureDate
        {
            get => _departureDate; set
            {
                _departureDate = value;
            }
        }
        public DateTime ReturnDate
        {
            get => _returnDate; set
            {
                if (value.CompareTo(DepartureDate) < 0)
                {
                    throw new DataInvalidException($"Return date must be after the departure date: {value:d}");
                }
                _returnDate = value;
            }
        }
    
}
}
