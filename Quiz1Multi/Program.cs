﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device;
using System.Device.Location;

namespace Quiz1Multi
{
    class Program
    {
        static List<Airport> AirportsList = new List<Airport>();
        const string FileName = @"..\..\data.txt";
        const string FileLogName = @"..\..\events.log";
        static string FilePath = Path.Combine(Environment.CurrentDirectory, FileName);
        static string FileLogPath = Path.Combine(Environment.CurrentDirectory, FileLogName);

        static void WriteDataToFile()
        {
            try
            {
                using (TextWriter tw = new StreamWriter(FilePath))
                {
                    foreach (var a in AirportsList)
                    {
                        tw.WriteLine(a.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {

                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        static void ReadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(FilePath))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            AirportsList.Add(new Airport(ln));
                        }
                        catch (InvalidDataException ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    { // show errors to the user
                        //Console.WriteLine("List of found errors:");
                        //Console.WriteLine(String.Join("\n", errorsList));
                        //LogFailSet?.Invoke(String.Join("\n", errorsList));
                        //Console.WriteLine("Press any key to continue");
                        //Console.ReadKey();
                        //Console.Clear();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }
        public static void LogToConsole(string msg) 
        {
            Console.WriteLine(msg);
        }
        public static void LogToFile(string msg) 
        {
            try
            {
                File.AppendAllText(FileLogPath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + msg + "\n");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        static int GetMenuChoice()
        {
            while (true)
            {
            Console.Write(
@"Menu:
1. Add Airport
2. List all airports
3. Find nearest airport by code (preferably using LINQ)
4. Find airport's elevation standard deviation (using LINQ)
5. Change log delegates
0. Exit
Enter your choice: ");
            string choiceStr = Console.ReadLine();
            Console.Clear();
            int choice;
            if (!int.TryParse(choiceStr, out choice) || choice < 0 || choice > 5)
            {
                Console.WriteLine("Value must be a number between 0-5");
                continue;
            }
            return choice;
            }
        }

        static void ChangeLoggingChoice()
        {
                Console.Write(
    @"Menu:
1-Logging to console
2-Logging to file
Enter your choices, comma-separated, empty for none: ");
                string choiceStr = Console.ReadLine();
                Console.Clear();
                if (choiceStr == "1")
                {
                Airport.Logger = LogToConsole;
                }
                if (choiceStr == "2")
                {
                Airport.Logger = LogToFile;
            }
                if (choiceStr == "1,2")
                {
                Airport.Logger = LogToConsole;
                Airport.Logger += LogToFile;
            }
                if (choiceStr == "")
                {
                }
        }

        static void AddAirport()
        {
            try
            {
                Console.Write("Adding airport");
                Console.Write("Enter code: ");
                string code = Console.ReadLine();
                Console.Write("Enter city: ");
                string city = Console.ReadLine();
                Console.Write("Enter latitude: ");
                double latitude = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter longitude: ");
                double longitude = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter elevation in meters: ");
                int elevation = Convert.ToInt32(Console.ReadLine());
                AirportsList.Add(new Airport(code, city, latitude, longitude, elevation));
                Console.WriteLine("Airport added.");
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ListAllAirports()
        {
            foreach (Airport a in AirportsList)
            {
                Console.WriteLine(a);
            }
        }

        static void FindAirportByCode()
        {
            Console.Write("Enter airport code: ");
            string code = Console.ReadLine();
            var sCoord = new GeoCoordinate();
            var eCoord = new GeoCoordinate();
            double min = 0;
            string foundAirport = "";
            string foundCity = "";
            foreach (Airport a in AirportsList)
            {
                eCoord = new GeoCoordinate(a.Latitude, a.Longitude);
                if (a.Code.ToLower().Contains(code.ToLower()))
                {
                    sCoord = new GeoCoordinate(a.Latitude, a.Longitude);
                }
                if (min < sCoord.GetDistanceTo(eCoord)) 
                {
                    min = sCoord.GetDistanceTo(eCoord)/1000;
                    foundAirport = a.Code;
                    foundCity = a.City;
                }
            }
            Console.Write($"Found nearest airport to be {foundAirport}/{foundCity} distance is {Math.Round(min,2)}km\n");
        }

        static List<double> GetElevList(List<Airport> airportList)
        {
            var elevList = new List<double>();
            Airport airport = null;
            foreach (var p in airportList)
            {
                    airport = (Airport)p;
                    elevList.Add(airport.ElevationMeters);
            }
            return elevList;
        }
        static double CalculateAverage(List<Airport> airportList)
        {
            // calculate average student's GPA
            var elevList = new List<double>();
            double elevAverage = 0;
            Airport airport = null;
            foreach (var p in airportList)
            {
               // if (p.GetType().Name == "Student")
                {
                    if (p is Airport)
                    {
                        airport = (Airport)p;
                    }
                    elevList.Add(airport.ElevationMeters);
                    elevAverage += airport.ElevationMeters;
                }
            }
            return elevAverage / elevList.Count;
        }

      

        static double CalculateGpaDeviation(List<Airport> airportList)
        {
            var devList = new List<double>();
            double eachDev = 0;
            double variance = 0;
            double standartDeviation = 0;
            foreach (double gpa in GetElevList(airportList))
            {
                eachDev = Math.Pow((gpa - CalculateAverage(airportList)), 2);
                devList.Add(eachDev);
                variance += eachDev;
            }
            variance = variance / devList.Count;
            // standartDeviation = Math.Round(Math.Sqrt(variance),2);
            standartDeviation = Math.Sqrt(variance);
            return standartDeviation;
        }
        static void Main(string[] args)
        {
            ReadDataFromFile();
            while (true)
            {
                int choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddAirport();
                        break;
                    case 2:
                        ListAllAirports();
                        break;
                    case 3:
                        FindAirportByCode();
                        break;
                    case 4:
                        Console.WriteLine(CalculateGpaDeviation(AirportsList)); 
                        break;
                    case 5:
                        ChangeLoggingChoice();
                        break;
                    case 0:
                        WriteDataToFile();
                        Console.WriteLine("Data saved. Exiting");
                        Environment.Exit(2);
                        break;
                    default:
                        Console.WriteLine("Internal error: invalid control flow in menu");
                        break;
                }
            }
        }
    }
}
