﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2EF
{
    /// <summary>
    /// Interaction logic for AddPassportDialog.xaml
    /// </summary>
    public partial class AddPassportDialog : Window
    {
        byte[] currPersonPhoto;
        Person currPerson;
        internal AddPassportDialog(Person person)
        {
            InitializeComponent();
            currPerson = person;
            lblName.Content = currPerson.Name;
            if (currPerson.Passport != null)
            {
                tbPassport.Text = currPerson.Passport.PassportNo;
                imageViewer.Source = Utils.GetBitmapImage(currPerson.Passport.Photo);
                currPersonPhoto = currPerson.Passport.Photo;
                btnSave.Content = "Update";
            }
        }

        public bool IsDialogFieldsValid()
        {
            Regex regex = new Regex(@"([A-Z]{2}[0-9]{6})$");
            Match match = regex.Match(tbPassport.Text);
            if (!match.Success)
            {
                MessageBox.Show("Passport Number wrong format (AB123456)", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (currPersonPhoto == null)
            {
                MessageBox.Show("Choose a photo", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currPersonPhoto = File.ReadAllBytes(dlg.FileName);
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.GetBitmapImage(currPersonPhoto); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            try
            {
                if (currPerson.Passport != null)
                {
                    currPerson.Passport.PassportNo = tbPassport.Text;
                    currPerson.Passport.Photo = currPersonPhoto;
                    Globals.ctx.SaveChanges();
                    DialogResult = true;
                    return;
                }
                Passport passport = new Passport
                {
                    Id = currPerson.Id,
                    PassportNo = tbPassport.Text,
                    Photo = currPersonPhoto
                };
                Globals.ctx.Passports.Add(passport);
                Globals.ctx.SaveChanges();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
