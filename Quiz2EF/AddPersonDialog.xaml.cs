﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2EF
{
    /// <summary>
    /// Interaction logic for AddPersonDialog.xaml
    /// </summary>
    public partial class AddPersonDialog : Window
    {
        public AddPersonDialog()
        {
            InitializeComponent();
        }

        public void ClearDialogInputs()
        {
            tbName.Text = "";
            tbAge.Text = "";
        }

        public bool IsDialogFieldsValid()
        {
            if (tbName.Text.Length < 1 || tbAge.Text.Length < 1)
            {
                MessageBox.Show("Please, fill in the fields", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age must be an integer", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            try
            {
                int.TryParse(tbAge.Text, out int age);
                Person p = new Person
                {
                    Name = tbName.Text,
                    Age = age
                };
                Globals.ctx.People.Add(p);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
