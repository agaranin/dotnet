﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2EF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new Quiz2DbContext();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
        }

        public void FetchRecords()
        {
            try
            {
                lvPeople.ItemsSource = Globals.ctx.People.Include("Passport").ToList();
                Utils.AutoResizeColumns(lvPeople);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MenuItemAddPerson_Click(object sender, RoutedEventArgs e)
        {
            AddPersonDialog addPersonDialog = new AddPersonDialog() { Owner = this };
            addPersonDialog.ShowDialog();
            FetchRecords();
        }

        private void lvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Person p = (Person)lvPeople.SelectedItem;
            if (p == null) { return; }
                AddPassportDialog addPassportDialog = new AddPassportDialog(p) { Owner = this };
                addPassportDialog.ShowDialog();
        }
    }
}
