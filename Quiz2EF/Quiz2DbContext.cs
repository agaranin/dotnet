﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2EF
{
    class Quiz2DbContext: DbContext
    {
        public Quiz2DbContext() : base("Quiz2EFDb") { }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Passport> Passports { get; set; }
    }
}
