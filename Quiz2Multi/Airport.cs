﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2Multi
{
    class Airport
    {
        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate Logger;

        string _code; // exactly 3 uppercase letters, use regexp
        string _city; // 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
        double _latitude, _longitude, _distance; // -90 to 90, -180 to 180
        int _elevationMeters;// -1000 to 10000

        public Airport(string code, string city, double latitude, double longitude, int elevationMeters)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
            ElevationMeters = elevationMeters;
            Logger?.Invoke("Logger: airport: " + this.ToDataString());
        }

        public Airport(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Invalid number of fields");
            }
            Code = data[0];
            City = data[1];
            double latitude;
            if (!double.TryParse(data[2], out latitude))
            {
                throw new InvalidDataException("Latitude must be a double value");
            }
            Latitude = latitude;
            double longitude;
            if (!double.TryParse(data[3], out longitude))
            {
                throw new InvalidDataException("Longitude must be a double value");
            }
            Longitude = longitude;
            int elevationMeters;
            if (!int.TryParse(data[4], out elevationMeters))
            {
                throw new InvalidDataException("ElevationMeters must be an integer value");
            }
            ElevationMeters = elevationMeters;
            Logger?.Invoke("Logger: airport: " + this.ToDataString());
        }



        public string Code
        {
            get => _code; set
            {
                if (!Regex.Match(value, @"\A[A-Z]{3}\Z").Success)
                {
                    Logger?.Invoke("Logger: Code must be  exactly 3 uppercase letters " + value);
                    throw new InvalidDataException("Code must be  exactly 3 uppercase letters");
                }
                _code = value;
            }
        }
        public string City
        {
            get => _city; set
            {
                if (!Regex.Match(value, @"\A[\sa-zA-Z0-9,.-]{1,50}\Z").Success)
                {
                    Logger?.Invoke("Logger: City must be between 1 and 50 characters, made up of uppercase and lowercase letters, digits, and., - " + value);
                    throw new InvalidDataException("City must be between 1 and 50 characters,  made up of uppercase and lowercase letters, digits, and .,-");
                }
                _city = value;
            }
        }
        public double Latitude
        {
            get => _latitude; set
            {
                if (value > 90 || value < -90)
                {
                    Logger?.Invoke("Logger: Latitude must be between -90 and 90 " + value);
                    throw new InvalidDataException("Latitude must be between -90 and 90 ");
                }
                _latitude = value;
            }
        }
        public double Longitude
        {
            get => _longitude; set
            {
                if (value > 180 || value < -180)
                {
                    Logger?.Invoke("Logger: Longitude must be between -90 and 90 " + value);
                    throw new InvalidDataException("Longitude must be between -90 and 90 ");
                }
                _longitude = value;
            }
        }
        public int ElevationMeters
        {
            get => _elevationMeters; set
            {
                if (value > 10000 || value < -1000)
                {
                    Logger?.Invoke("Logger: ElevationMeters must be between -1000 and 10000 " + value);
                    throw new InvalidDataException("ElevationMeters must be between -1000 and 10000 ");
                }
                _elevationMeters = value;
            }
        }
        

        public double Distance
        {
            get
            {
                return _distance;
               // return 100;
            }

            set
            {
                _distance = value;
            }
        }

        public override string ToString()
        {
            return string.Format($"{Code} in {City} at {Latitude} lat / {Longitude} lng at {ElevationMeters}m elevation");
        }

        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
    }
}
