﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Device.Location;

namespace Quiz2Multi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Airport> AirportList = new List<Airport>();
        const string FileName = @"..\..\data.txt";
        public MainWindow()
        {
            InitializeComponent();
            ReadDataFromFile();
            comboAirport.ItemsSource = AirportList;
            comboAirport.DisplayMemberPath = "Code";
            CalcDistance();
            lvAirports.ItemsSource = AirportList;
            lvAirports.Items.Refresh();
            lblStatusBar.Text = "Airport's elevation standard deviation: " + CalculateDeviation();
        }

        static void ReadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(FileName))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            AirportList.Add(new Airport(ln));
                        }
                        catch (InvalidDataException ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    { 
                        MessageBox.Show(String.Join("\n", errorsList));
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbCode.Text == "" || tbCity.Text == "" || tbLatitude.Text == "" || tbLongitude.Text == "" || tbElevation.Text == "")
            {
                MessageBox.Show("Please fill all fields");
                return;
            }
            string code = tbCode.Text;
            string city = tbCity.Text;
            if (!double.TryParse(tbLatitude.Text, out double latitude))
            {
                MessageBox.Show("Lattitude must be a double value");
                return;
            }
            if (!double.TryParse(tbLongitude.Text, out double longitude))
            {
                MessageBox.Show("Longitude must be a double value");
                return;
            }
            if (!int.TryParse(tbElevation.Text, out int elevation))
            {
                MessageBox.Show("Elevation must be an integer value");
                return;
            }
            try
            {
                AirportList.Add(new Airport(code, city, latitude, longitude, elevation));
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            lvAirports.Items.Refresh();
            ClearInputs();
        }
        private void ClearInputs()
        {
            tbCode.Text = "";
            tbCity.Text = "";
            tbLatitude.Text = "";
            tbLongitude.Text = "";
            tbElevation.Text = "";
        }

        public void CalcDistance()
        {
            Airport selAir = (Airport)comboAirport.SelectedItem;
            var selAirCoord = new GeoCoordinate(selAir.Latitude, selAir.Longitude);
            foreach (Airport a in AirportList)
            {
                var nextCoord = new GeoCoordinate(a.Latitude, selAir.Longitude);
                a.Distance = selAirCoord.GetDistanceTo(nextCoord) / 1000;
                }
            }

        private void comboAirport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CalcDistance();
            lvAirports.Items.Refresh();
        }

        static double CalculateDeviation()
        {
            double average = AirportList.Sum(a => a.ElevationMeters) / AirportList.Count;
            double variance = 0;
            foreach (Airport a in AirportList)
            {
                variance += Math.Pow((a.ElevationMeters - average), 2);
            }
            variance /= AirportList.Count;
            return Math.Sqrt(variance);
        }
    }
    }

