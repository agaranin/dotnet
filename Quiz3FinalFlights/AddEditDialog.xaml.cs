﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz3FinalFlights
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Flight currFlight;
        public AddEditDialog(Flight flight)
        {
            InitializeComponent();
            comboType.ItemsSource = Enum.GetValues(typeof(Flight.TypeEnum));
            comboType.SelectedIndex = 0;
            btnSave.Content = "Add";
            if (flight != null) // update flight
            {
                currFlight = flight;
                lblId.Content = flight.Id;
                dpOnDate.SelectedDate = flight.OnDay;
                tbFromCode.Text = flight.FromCode;
                tbToCode.Text = flight.ToCode;
                comboType.SelectedItem = flight.Type;
                sliderPassengers.Value = flight.Passengers;
                btnSave.Content = "Update";
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime onDay = dpOnDate.SelectedDate.GetValueOrDefault();
                string fromCode = tbFromCode.Text;
                string toCode = tbToCode.Text;
                Flight.TypeEnum type = (Flight.TypeEnum)comboType.SelectedItem;
                int passengers = (int)sliderPassengers.Value;
                if (currFlight != null) // update flight
                {
                    Globals.db.UpdateFlight(new Flight(currFlight.Id, onDay, fromCode, toCode, type, passengers));
                }
                else
                {
                    int id = Globals.db.AddFlight(new Flight(0, onDay, fromCode, toCode, type, passengers));
                }
                DialogResult = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}
