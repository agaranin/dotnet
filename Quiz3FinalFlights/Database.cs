﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3FinalFlights
{
    internal class Database
    {
        const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Artem\Documents\dotnet\Quiz3FinalFlights\FlightsDb.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(ConnString);
            conn.Open(); //ex
        }

        public List<Flight> GetAllFlights() //ex
        {
            List<Flight> list = new List<Flight>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Flights", conn))
            using (SqlDataReader reader = cmd.ExecuteReader()) //ex
            {
                while (reader.Read())
                {
                    list.Add(new Flight(reader)); //ex
                }
            }
            return list;
        }

        public int AddFlight(Flight flight) //ex
        {
            using (SqlCommand cmd = new SqlCommand("INSERT INTO Flights (OnDay, FromCode, ToCode, Type, Passengers) VALUES (@OnDay, @FromCode, @ToCode, @Type, @Passengers) SELECT SCOPE_IDENTITY()", conn))
            {
                cmd.Parameters.AddWithValue("@OnDay", flight.OnDay);
                cmd.Parameters.AddWithValue("@FromCode", flight.FromCode);
                cmd.Parameters.AddWithValue("@ToCode", flight.ToCode);
                cmd.Parameters.AddWithValue("@Type", Enum.GetName(typeof(Flight.TypeEnum), flight.Type));
                cmd.Parameters.AddWithValue("@Passengers", flight.Passengers);
                return Convert.ToInt32(cmd.ExecuteScalar()); //ex
            }

        }
        public void UpdateFlight(Flight flight) //ex
        {
            using (SqlCommand cmd = new SqlCommand("UPDATE Flights SET OnDay=@OnDay, FromCode=@FromCode, ToCode=@ToCode, Type=@Type,  Passengers=@Passengers WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@OnDay", flight.OnDay);
                cmd.Parameters.AddWithValue("@FromCode", flight.FromCode);
                cmd.Parameters.AddWithValue("@ToCode", flight.ToCode);
                cmd.Parameters.AddWithValue("@Type", Enum.GetName(typeof(Flight.TypeEnum), flight.Type));
                cmd.Parameters.AddWithValue("@Passengers", flight.Passengers);
                cmd.Parameters.AddWithValue("@Id", flight.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }
        public void DeleteFlight(Flight flight) //ex
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Flights WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Id", flight.Id);
                cmd.ExecuteNonQuery(); // ex
            }
        }
    }
}
