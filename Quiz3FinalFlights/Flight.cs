﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz3FinalFlights
{
    public class Flight
    {
        public enum TypeEnum { Domestic, International, Private };

        int _id;
        DateTime _onDay;
        string _fromCode;
        string _toCode;
        TypeEnum _type;
        int _passengers;

        public Flight(int id, DateTime onDay, string fromCode, string toCode, TypeEnum type, int passengers)
        {
            Id = id;
            OnDay = onDay;
            FromCode = fromCode;
            ToCode = toCode;
            Type = type;
            Passengers = passengers;
        }

        public Flight(SqlDataReader reader) // ex SQL and Argument
        {
            Id = reader.GetInt32(reader.GetOrdinal("Id"));
            OnDay = reader.GetDateTime(reader.GetOrdinal("OnDay"));
            FromCode = reader.GetString(reader.GetOrdinal("FromCode"));
            ToCode = reader.GetString(reader.GetOrdinal("ToCode"));
            string typeStr = reader.GetString(reader.GetOrdinal("Type"));
            if (!Enum.TryParse<TypeEnum>(typeStr, out TypeEnum typeParsed))
            {
                throw new ArgumentException("Cannot parse Type");
            }
            Type = typeParsed;
            Passengers = reader.GetInt32(reader.GetOrdinal("Passengers"));
        }

        public int Id
        {
            get => _id; set
            {
                _id = value;
            }
        }
        public DateTime OnDay
        {
            get => _onDay; set
            {
                _onDay = value;
            }
        }
        public string FromCode
        {
            get => _fromCode; set
            {
                if (!Regex.Match(value, @"\A[A-Z]{3,5}\Z").Success)
                {
                    throw new ArgumentException("From code must be between 3 and 5 characters, uppercase: " + value);
                }
                _fromCode = value;
            }
        }
        public string ToCode
        {
            get => _toCode; set
            {
                if (!Regex.Match(value, @"\A[A-Z]{3,5}\Z").Success)
                {
                    throw new ArgumentException("To code must be between 3 and 5 characters, uppercase: " + value);
                }
                _toCode = value;
            }
        }
        public int Passengers
        {
            get => _passengers; set
            {
                _passengers = value;
            }
        }
        public TypeEnum Type { get => _type; set => _type = value; }

        

    }
}
