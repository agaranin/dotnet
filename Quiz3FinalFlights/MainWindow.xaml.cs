﻿using CsvHelper;
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3FinalFlights
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Flight> FlightsList = new List<Flight>();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.db = new Database();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error connecting to the Database" + ex.Message, "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
            ((INotifyCollectionChanged)lvFlights.Items).CollectionChanged += lvFlights_CollectionChanged;
            GetDataFromDatabase();
        }
        private void lvFlights_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            tbStatus.Text = $"Total Flights: {FlightsList.Count}";
        }

        public void GetDataFromDatabase()
        {
            try
            {
                FlightsList = Globals.db.GetAllFlights();
                lvFlights.ItemsSource = FlightsList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void EditItemCurrSelected()
        {
            Flight flight = (Flight)lvFlights.SelectedItem;
            if (flight == null) { return; }
            AddEditDialog addEditDilaog = new AddEditDialog(flight) { Owner = this };
            addEditDilaog.ShowDialog();
            GetDataFromDatabase();
        }

        private void MenuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog flightDialog = new AddEditDialog(null) { Owner = this };
            flightDialog.ShowDialog();
            GetDataFromDatabase();
        }

        private void lvFlights_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItemCurrSelected();
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvFlights.SelectedIndex == -1)
            {
                MessageBox.Show("Select item to delete", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                Flight flight = (Flight)lvFlights.SelectedItem;
                Globals.db.DeleteFlight(flight);
                GetDataFromDatabase();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemSaveSelected_Click_1(object sender, RoutedEventArgs e)
        {
            if (lvFlights.SelectedIndex != -1)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "CSV file (*.csv)|*.csv";
                saveFileDialog.Title = "Export to file";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        using (var writer = new StreamWriter(saveFileDialog.FileName))
                        using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                        {
                            var options = new TypeConverterOptions { Formats = new[] { "yyyy-MM-dd" } };
                            csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                            csv.Configuration.Delimiter = ";";
                            csv.WriteRecords(lvFlights.SelectedItems);
                        }
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            else
            {
                MessageBox.Show("Select items to export.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
