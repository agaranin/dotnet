﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz5SuperMix
{
    class Program
    {
        static SuperFibs sf;
        static void Main(string[] args)
        {
            sf = new SuperFibs();
            for (int i = 1; i < 11; i++)
            {
                Console.WriteLine($"prints {i}: {sf[i]}, steps: {sf.StepsCount}");
            }
            while (true)
            {
                GetFibByIndex();
            }
        }

        static void GetFibByIndex()
        {
            Console.WriteLine("Enter number to find nth Super Fibonacci number:");
            try
            {
                int nth = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"{nth}th Super Fibonacci number is {sf[nth]}, steps: {sf.StepsCount}");
            }
            catch (FormatException)
            {
                Console.WriteLine("Enter the number ");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                Console.Clear();
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }
    }
}
