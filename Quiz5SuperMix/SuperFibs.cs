﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz5SuperMix
{
    public class SuperFibs
    {
        private static int _stepsCount;
        static Dictionary<int, long> _memo { get; set; }
        public int StepsCount { get => _stepsCount; }

        public SuperFibs()
        {
            _memo = new Dictionary<int, long>
           () { { 0, 0 }, { 1, 1 }, {2, 2} };
            _stepsCount = 0;
        }

        //Part 1
        private static long SuperFib(int n)
        {
            if (n == 1){return 0;}
            if (n == 2){return 1;}
            if (n == 3){return 1;}
            else
            {
                
                return SuperFib(n - 1) + SuperFib(n - 2) + SuperFib(n - 3);
            }
        }
         
        //Part 2
        static long SuperFibMemo(int n)
        {
            
            if (n == 1) { return 0; }
            if (n == 2) { return 1; }
            if (n == 3) { return 1; }
            if (_memo.ContainsKey(n))
                return _memo[n];
            
            var value = SuperFibMemo(n - 1) + SuperFibMemo(n - 2) + SuperFibMemo(n - 3);
            _stepsCount++;
            _memo[n] = value;

            return value;
        }

        public long this[int nth]
        {
            get
            {
                if (nth <= 0)
                {
                    throw new IndexOutOfRangeException("Index cannot be less than 1");
                }
                return SuperFibMemo(nth);
            }
        }
    }
}
