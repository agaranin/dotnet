﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quiz5SuperMix;
using System;

namespace SuperFibsTests
{
    [TestClass]
    public class SuperFibs_UnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void NegativeIndexerShouldThrowException()
        {
            SuperFibs sf = new SuperFibs();
            var fib = sf[-1];
            Assert.Fail("Exception must happen");
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void ZeroIndexerShouldThrowException()
        {
            SuperFibs sf = new SuperFibs();
            var fib = sf[0];
            Assert.Fail("Exception must happen");
        }

        [TestMethod]
        public void IndexerReturnsSuperFibsValues()
        {
            SuperFibs sf = new SuperFibs();
            Assert.AreEqual(sf[1], 0);
            Assert.AreEqual(sf[2], 1);
            Assert.AreEqual(sf[3], 1);
            Assert.AreEqual(sf[5], 4);
            Assert.AreEqual(sf[7], 13);
            Assert.AreEqual(sf[9], 44);
        }

        [TestMethod]
        public void StepsCountReturns_7_CalculatingNinthSuperFibs ()
        {
            SuperFibs sf = new SuperFibs();
            var fib = sf[9];
            Assert.AreEqual(sf.StepsCount, 6);
           
        }
    }
}
